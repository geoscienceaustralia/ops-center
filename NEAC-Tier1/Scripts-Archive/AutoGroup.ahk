;
; Setup Functional Groups and Build HotKeys
;
; Description: Assumes that all windows have been correctly opened and arranged by AutoLayout script.
;
; Tony Pack - May 2018
;

;	Screen 7
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)")
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)")
;	Screen 2
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)")
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)")
;	Screen 8
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)")
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)")
;	Screen 3
	AddWindow( "Clock Panel")
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)")
	AddWindow( "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)")

;	Screen 9	
	AddWindow( "Integrated Geophysical Networks, Seismic Data Home Page")
;	Screen 4	
	AddWindow( "rhe-eqm-ops-prod.prod.lan/widescreen/latency.html")
	
NumpadDiv:: 
	; Hide AntelopeMain Group of Windows
	WinHide, ahk_group AntelopeMain
	Return
	
NumpadMult::
	; Show AntelopeMain Group of Windows
	WinShow, ahk_group AntelopeMain
	Return

AddWindow(Title)
{
	ifWinExist, %Title%
	{
		MsgBox, Found - %Title%
		WinActivate
		GroupAdd, AntelopeMain
	}
}