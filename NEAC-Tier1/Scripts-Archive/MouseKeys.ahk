;
; 	Mouse Hotkeys
;	
;	Tony Pack - January/July 2018
;
;	Utility for multiscreen display. Moves mouse to top left quadrant of selected screen.
;	
;	Number Lock has been disabled to ensure alway on
;	Duplicate Alt+Number keys enable to allow for non keypad keyboard - eg. AV Room
;

/*	Screen Matrix
---------------------------------------------------------------------
	EATWS 16 Output PC is configured as 16x 19100/1080 monitors
	Horizontal = 15360 by Vertical = 2160
*/

global Mode := "Local"			; Set to "Test" for 2 Screen Test Environment: "Local" for EATWS VideoWall

; 	Get Monitor Array Information:
; --------------------------------------------------------- 
; - Number of monitors reported by system
SysGet, MonitorCount, MonitorCount
NumMonitors := MonitorCount

SysGet, VirtualScreenWidth, 78
SysGet, VirtualScreenHeight, 79
MaxScreenWidth := VirtualScreenWidth
MaxScreenHeight := VirtualScreenHeight

; - Monitors/Grid for virtual layout
xGrid := 8
yGrid := 2

; - Width/Height of virtual monitors
xStep := Ceil(MaxScreenWidth/xGrid)
yStep := Ceil(MaxScreenHeight/yGrid)


sendCtrl(wait:=250) {
	Send {Ctrl down}{Ctrl up}
	Sleep %wait%
	Send {Ctrl down}{Ctrl up}
}


;	Load Arrays:
;	===========================================================================
	global WinCoords := []		; Size and Coordinates for Windows
	Load_WinCoords(Mode)

;	Load Mousekey Definitions:
;	===========================================================================
SetNumLockState, AlwaysOn		; Forces NumLock to be always on - for keypad mouse keys

^!Backspace::
	; Control + Alt + Backspace = Deleted contents of Startup Folder and Restarts
	MsgBox, 1,, Force Reboot
	IfMsgBox OK
	{
		; Force a reboot (reboot + force = 2 + 4 = 6):
		Shutdown, 6
	}
	return	

NumpadDot::
!`::
	; Keypad Decimal Point/Delete - PC Screen 1 - Input 8 - 0/0
	DllCall("SetCursorPos", int, 100, int, 100)
	SendCtrl()
	return	
	
Numpad1::
!1::
	; Keypad 1 - PC Screen 1 - Input 8 - 0/0
	DllCall("SetCursorPos", int, WinCoords[1].x + 100, int, WinCoords[1].y + 100)
	SendCtrl()
	return

Numpad2::
!2::
	; Keypad 2 - PC Screen 2 - Input 2 - 19100/0
	DllCall("SetCursorPos", int, WinCoords[2].x + 100, int, WinCoords[2].y + 100)
	SendCtrl()
	return

Numpad3::
!3::
	; Keypad 3 - PC Screen 3 - Input 4 - 3840/0
	DllCall("SetCursorPos", int, WinCoords[3].x + 100, int, WinCoords[3].y + 100)
	SendCtrl()
	return

Numpad4::
!4::
	; Keypad 4 - PC Screen 4 - Input 4 - 5760/0
	DllCall("SetCursorPos", int, WinCoords[4].x + 100, int, WinCoords[4].y + 100)
	SendCtrl()
	return

Numpad5::
!5::
	; Keypad 5 - PC Screen 5 - Input 8 - 7680/0
	DllCall("SetCursorPos", int, WinCoords[5].x + 100, int, WinCoords[5].y + 100)
	SendCtrl()
	return

; ------------ Top Row of Screens ---------
Numpad6::
!6::
	; Keypad 6 - PC Screen 6 - Input 12 - 0/-1080
	DllCall("SetCursorPos", int, WinCoords[6].x + 100, int, WinCoords[6].y + 100)
	SendCtrl()
	return

Numpad7::
!7::
	; Keypad 7 - PC Screen 7 - Input 1 - 19100/-1080
	DllCall("SetCursorPos", int, WinCoords[7].x + 100, int, WinCoords[7].y + 100)
	SendCtrl()
	return

Numpad8::
!8::
	; Keypad 8 - PC Screen 8 - Input 3 - 3840/-1080
	DllCall("SetCursorPos", int, WinCoords[8].x + 100, int, WinCoords[8].y + 100)
	SendCtrl()
	return

Numpad9::
!9::
	; Keypad 9 - PC Screen 9 - Input 5 - 5760/-1080
	DllCall("SetCursorPos", int, WinCoords[9].x + 100, int, WinCoords[9].y + 100)
	SendCtrl()
	return

Numpad0::
!0::
	; Keypad 0 - PC Screen 10 - Input 10 - 7680/-1080
	DllCall("SetCursorPos", int, WinCoords[10].x + 100, int, WinCoords[10].y + 100)
	SendCtrl()
	return

	
Load_WinCoords(Mode)
{
	;	Screen/Window Coordinates:
	;	===========================================================================
	;	WinCoords := []		initiate the array object in Head before calling function
	;	WinCoords [ScreenNo].x & [ScreenNo].y = Top Left Coordinate of Window
	;	WinCoords[0].width 	= Width 
	;	WinCoords[0].height	= Height
	;	
	;	EATWS Video Wall = 8 x 2 = 16 Monitors
	;		Width = 8 x 1920 = 15360
	;		Height = 2 x 1080 = 2160
	;
	;	2 Screen Test Mode = 2 Monitors
	;		Width = 2 x 1920 = 3840
	;		Height = 1080

	If DebugLev2
		MsgBox % "In Load_WinCoords with Mode = " . Mode

	if (Mode = "Local")
	{
		StepX := 1920
		StepY := 1080
		BaseY := -1080	; was 0
		TopY  := -2160
	}
	else if (Mode = "Test")
	{	; for dual monitors a suitable test mode is 640 x 540
		StepX := 680
		StepY := 540
		BaseY := 540
		TopY  := 0
	}
	
	If DebugLev2
		MsgBox % "Load_WinCoords: StepX,StepY,BaseY,TopY: " . StepX . "," . StepY . "," . BaseY . "," . TopY
			
	;Screens 	Width and Height
	;-------------------------------------------------
	WinCoords[0] := {width: StepX, height: StepY}

	;Bottom VideoWall Screens 	1 to 5 (Left to Right)
	;-------------------------------------------------
	WinCoords[1] 	:= {x: StepX * 0, y: BaseY}	; Bottom Left: Y = 0 or 540
	WinCoords[2] 	:= {x: StepX * 1, y: BaseY}
	WinCoords[3] 	:= {x: StepX * 2, y: BaseY}
	WinCoords[4] 	:= {x: StepX * 3, y: BaseY}
	WinCoords[5] 	:= {x: StepX * 4, y: BaseY}	; Bottom Right - Placements X+480, Y=270

	; Top VideoWall Screens 		6 to 10 (Left to Right)
	; --------------------------------------------------
	; Top of Bottom Screen = 0 / Top of Top Screen = -1080
	WinCoords[6] 	:= {x: StepX * 0, y: TopY}	; Top Left: Y = -1080 or 0
	WinCoords[7] 	:= {x: StepX * 1, y: TopY}
	WinCoords[8] 	:= {x: StepX * 2, y: TopY}
	WinCoords[9] 	:= {x: StepX * 3, y: TopY}
	WinCoords[10] 	:= {x: StepX * 4, y: TopY}	; Top Right

	;Non-Visible Screens 11 to 16
	;--------------------------------------------
	; Third In from Far Right	11=Top/12=Bottom
	WinCoords[11] 	:= {x: StepX * 5, y: TopY}
	WinCoords[12] 	:= {x: StepX * 5, y: BaseY}
	;--------------------------------------------
	; Second In from Far Right	13=Top/14=Bottom
	WinCoords[13] 	:= {x: StepX * 6, y: TopY}
	WinCoords[14] 	:= {x: StepX * 6, y: BaseY}
	;--------------------------------------------
	;Far Right					15=Top/16=Bottom
	WinCoords[15] 	:= {x: StepX * 7, y: TopY}
	WinCoords[16] 	:= {x: StepX * 7, y: BaseY}
	;--------------------------------------------
	
	Return 	; WinCoords[]
}

/*
; 	------------ Hidden Screens - 11 to 16 ---------
;	Hidden Screens have been disabled - June 2018

+NumpadEnd::
;^1::
	; SHIFT + Keypad 1 - PC Screen 11 - Input 13 - 9600/-1080
	DllCall("SetCursorPos", int, WinCoords[11].x + 100, int, WinCoords[11].y + 100)
	SendCtrl()
	return

;^2::
+NumpadDown::
	; SHIFT + Keypad 2 - PC Screen 12 - Input 9 - 9600/0
	DllCall("SetCursorPos", int, WinCoords[12].x + 100, int, WinCoords[12].y + 100)
	SendCtrl()
	return

;^3::
+NumpadPgDn::
	; SHIFT + Keypad 3 - PC Screen 13 - Input 11 - 115100/-1080
	DllCall("SetCursorPos", int, WinCoords[13].x + 100, int, WinCoords[13].y + 100)
	SendCtrl()
	return

;^4::
+NumpadLeft::
	; SHIFT + Keypad 4 - PC Screen 14 - Input 14 - 115100/0
	DllCall("SetCursorPos", int, WinCoords[14].x + 100, int, WinCoords[14].y + 100)
	SendCtrl()
	return

;^5::
+NumpadClear::
	; SHIFT + Keypad 5 - PC Screen 15 - Input 7 - 13440/-1080
	DllCall("SetCursorPos", int, WinCoords[15].x + 100, int, WinCoords[15].y + 100)
	SendCtrl()
	return

+NumpadRight::
	; SHIFT + Keypad 6 - PC Screen 16 - Input 15 - 13440/0
	DllCall("SetCursorPos", int, WinCoords[16].x + 100, int, WinCoords[16].y + 100)
	SendCtrl()
	return
*/