;	===========================================================================
;
; 	Display Scripts Library File
;
; 	Description: Common Window Opening/Positioning Routines
;
; 	Tony Pack - May 2018
;
;	Uses global variable WinWaitTime
;	===========================================================================

Display_CMDWindow(x,y,width,height,GroupName)
{
	; Window Title not known
	
	If DebugLev2
		MsgBox, [In-CMD] xPos/yPos: %x%/%y%, width/yStep: %width%/%height%
		
	Run, cmd.exe,,,OutputVarPID
	WinWait, ahk_pid %OutputVarPID%, , %WinWaitTime%
	WinMove, ahk_pid %OutputVarPID%,,%x%,%y%,%width%,%height%
	if %GroupName%
		GroupAdd, %GroupName%, ahk_pid %OutputVarPID%
}

Display_LNKWindow(x,y,width,height,LinkName,TitleName,GroupName)
{	
	If WinExist(TitleName)
	{
		WinClose  	; Automatically uses the window found above.
	}

	If !WinExist(TitleName)	; Check that window is not already open - if not then open link
	{
		If DebugLev2
			MsgBox, Window Not Found (Opening): [In-LNK] Title: %TitleName% xPos/yPos: %x%/%y% width/height: %width%/%height%
		Run, %LinkName%
		WinWait, %TitleName%, , %WinWaitTime%
		WinActivate %TitleName%
	}

	If WinExist(TitleName)
	{
		WinActivate  	; Automatically uses the window found above.
		WinMove, %TitleName%,,%x%,%y%,%width%,%height%
		if %GroupName%
			GroupAdd, %GroupName%, %TitleName%
	}
}

Display_OpenWindow(x,y,width,height, TitleName, GroupName)
{	
	; Activates/Moves Already Open Window
	; Requires TitleName to find/activate Window
	
	If WinExist(TitleName)
	{
		If DebugLev2
			MsgBox, Window Found: [In-OPEN] Title: %TitleName% xPos/yPos: %x%/%y% width/height: %width%/%height%
			
		WinActivate  	; Automatically uses the window found above.
		WinMove, %TitleName%,,%x%,%y%,%width%,%height%
		if %GroupName%
			GroupAdd, %GroupName%, %TitleName%
	}
	else
	{
		If DebugLev2
			MsgBox, Window Not Found: [In-OPEN] Title: %TitleName% xPos/yPos: %x%/%y% width/height: %width%/%height%
	}
}

Display_SelectButton(BtnX,BtnYBase,BtnYStep,BtnOption,Speed)
{
	; Select Button in window - eg. to select Region view in Earthquake or Station Latency views.
	
	Option := BtnYBase + ( BtnOption * BtnYStep )
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %BtnYBase%, %Speed%	
	MouseMove, %BtnX%, %BtnYBase%, %Speed%
	Click, Left
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %Option%, %Speed% 
	MouseMove, %BtnX%, %Option%, %Speed% 
	Click, Left
}