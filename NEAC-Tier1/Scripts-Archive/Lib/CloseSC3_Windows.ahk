;	===========================================================================
;
; 	Close SC3 Windows Scripts Library File
;
; 	Description: 
;
; 	Tony Pack - July 2018
;
;	===========================================================================

;	Close any existing scrttv views
;	===================================================	

;	Close windows from other SC3 
	
	Progress, 30, Close Existing MobaXterm and SeisComP3 Waveforms windows
	Sleep 1000
	instNum := 0
	Loop
	{
		Process,Exist,MoTTY.exe
		If ErrorLevel
		{	
			++instNum
			If DebugLev2
				MsgBox, Found MoTTY #%instNum%
			Process,Close,%Errorlevel%
		} else 
		{
			If DebugLev2
				MsgBox, No more MoTTY instances
			break
		}
	}

	instNum := 0
	Loop
	{
		Process,Exist,MobaXterm_Personal_10.2.exe
		If ErrorLevel
		{	
			++instNum
			If DebugLev2
				MsgBox, Found MobaXterm_Personal_10 instance #%instNum%
			Process,Close,%Errorlevel%
		} else 
		{
			If DebugLev2
				MsgBox, No more MobaXterm_Personal_10 instances
			break
		}
	}
		
/*	WinGet, SC3WinList , List, scrttv
	
	Loop, %SC3WinList% 
	{
		WinActivate, % "ahk_id " SC3WinList%A_Index%
		If DebugLev2
			MsgBox, This is window %A_Index% in the list.
		WinClose, % "ahk_id " SC3WinList%A_Index%
	}
*/