;
; 	SC3Group_HotKeys.ahk
;
; 	Description: 	Defines ahk HotKeys to Hide/Show Antelope Group of Windows.
;				
; 	Requires:		List of Group IDs passed from calling script. As a single string with comma separated ID values
;
; 	Tony Pack - May 2018
;	===========================================================================


#SingleInstance force		; Replaces any old instance of scripts automatically
global LayoutGroupName := "SC3_Group"

	GroupList = %1%
	;MsgBox, GroupList: . %GroupList%

	Loop, parse, GroupList, `,
	{
		;MsgBox, Window number %A_Index% is %A_LoopField%
		GroupAdd, %LayoutGroupName%, ahk_id %A_LoopField%
	}	

^+Q::
	WinHide, ahk_group %LayoutGroupName%
	Return
	
^+W::
	WinShow, ahk_group %LayoutGroupName%
	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	Loop % GroupList
	{
		WinShow , ahk_id GroupList%A_Index%
		WinActivate , ahk_id GroupList%A_Index%
		; MsgBox % "Window: " . %A_Index% . " - " . GroupList%A_Index%
	}
	return

^+E::
	WinClose, ahk_group %LayoutGroupName%
	ExitApp
	Return