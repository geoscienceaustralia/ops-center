;
; Tier 2 AutoScreens Layout
;
; Description: Positions Waveform and Title Screen on Startup
;
; Tony Pack - January 2018
;

global Debug := False
global DebugLev2 := False
global Mode := Local
global RunFull := False
global ExcludeID := ""

global ShortCutDir := "C:\Users\jawtc\Desktop\"


; Monitor Array:
; --------------------------------------------------------- 
; - Number of monitors reported by system
SysGet, MonitorCount, MonitorCount
NumMonitors := MonitorCount

SysGet, VirtualScreenWidth, 78
SysGet, VirtualScreenHeight, 79
MaxScreenWidth := VirtualScreenWidth
MaxScreenHeight := VirtualScreenHeight

; - Monitors/Grid for virtual layout
xGrid := 2
yGrid := 2

; - Width/Height of virtual monitors
xStep := Ceil(MaxScreenWidth/xGrid)
yStep := Ceil(MaxScreenHeight/yGrid)

If Debug
{
	MsgBox, 
	(
		Monitor Array Setup:
		ShortCutDir: %ShortCutDir%
		Number of Monitors: %MonitorCount%
		Screen: %MaxScreenWidth%/%MaxScreenHeight%
		xStep/yStep: %xStep%/%yStep%
	)
	;MsgBox, Screen: %MaxScreenWidth%/%MaxScreenHeight%
	;MsgBox, xStep/yStep: %xStep%/%yStep%
}

;	Layout of Screen for Default Inputs
;---------------------------------------------------------------------

; Wait until Antelope Screens have loaded
	WinWait, "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)", , 5

; Waveforms - Tier2
	Monitor_1_4( 1920, -1080, 1920, 1080)		;[Input 22 - Screen 8] - Tier 2 Waveforms
	Clock_Panel( 1920, -1100, 1920, 90)		;[3 - header]

;---------------------------------------------------------------------

	
CMDWindow(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-CMD] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Run, cmd.exe,,,OutputVarPID
	WinWait, ahk_pid %OutputVarPID%
	xStep := width
	yStep := height
	WinMove, ahk_pid %OutputVarPID%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, ahk_pid %OutputVarPID%
}

LNKWindow(x,y,width,height,LinkName, TitleName)
{	
	If DebugLev2
		MsgBox, 
		(
			[In-LNK] xPos/yPos: %x%/%y%, 
			xStep/yStep: %width%/%height%, 
			Link: %LinkName%, 
			Title: %TitleName%
			ExludeID: %ExcludeID%
		)

	IfWinNotExist, %TitleName%
	{
		Run, %LinkName%
	}

	if %ExcludeID%	;Any Value
	{
		If DebugLev2
			MsgBox, ExcludeID is true = Any Value: %ExcludeID%
		WinWait, %TitleName%,,,ahk_id %ExcludeID%
	} else {
		If DebugLev2
			MsgBox, ExcludeID is False = no value: %ExcludeID%
		WinWait, %TitleName%
	}

	xStep := width
	yStep := height
	WinMove, %TitleName%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, A
}

OpenWindow(x,y,width,height, TitleName)
{	;Activates/Moves Already Open Window

	If DebugLev2
		MsgBox, [In-LNK] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%, Title: %TitleName%
		
	WinActivate, %TitleName%

	xStep := width
	yStep := height
	WinMove, %TitleName%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, A
}


Monitor_1_4(x,y,width,height)		; Waveforms Tier 2 Regional
{
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 70

	; ASK Sachin to rename window as for Video Wall bash file...

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)"
	OpenWindow(x,y+VOffset,width/2,height-Voffset, Title)

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)"
	OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title)
}

Clock_Panel(x,y,width,height)	; Actually Title Pane for Tier 2
{
	If DebugLev2
		MsgBox, [In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Link := ShortCutDir . "Tier2-TitleBar.lnk"
	Title := "Tier2"
	LNKWindow(x, y, width, height, Link, Title)
}


SelectButton(BtnX,BtnYBase,BtnYStep,BtnOption,Speed)
{
	Option := BtnYBase + ( BtnOption * BtnYStep )
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %BtnYBase%, %Speed%	
	MouseMove, %BtnX%, %BtnYBase%, %Speed%
	Click, Left
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %Option%, %Speed% 
	MouseMove, %BtnX%, %Option%, %Speed% 
	Click, Left
}