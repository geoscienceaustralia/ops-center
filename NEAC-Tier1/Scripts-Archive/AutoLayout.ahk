;
; PC Main: VideoWall Screens Layout
;
; Description: Positions Antelope Waveform, Events and Station Latency views along with Title Screens and SeisComP3 screens on Startup
;
; Tony Pack - January 2018
;


global Debug := False
global DebugLev2 := False
global Mode := Local
global RunFull := False
global ExcludeID := ""

;global ShortCutDir := "C:\Users\tonyp\Desktop\"
;global ShortCutDir := "C:\Users\u69679\Desktop\"
global ShortCutDir := "C:\Users\jatwc-new\Desktop\"


; Monitor Array:
; --------------------------------------------------------- 
; - Number of monitors reported by system
SysGet, MonitorCount, MonitorCount
NumMonitors := MonitorCount

SysGet, VirtualScreenWidth, 78
SysGet, VirtualScreenHeight, 79
MaxScreenWidth := VirtualScreenWidth
MaxScreenHeight := VirtualScreenHeight

;OverWrite - Screen Dimensions
; Default: 1920 x 1080
; (10) 5 x 2 =  9600 x 2160
; (16) 8 x 2 = 15360 x 2160
MaxScreenWidth := 3840
MaxScreenHeight := 2160


; - Monitors/Grid for virtual layout
xGrid := 5	
yGrid := 2

; - Width/Height of virtual monitors
xStep := Ceil(MaxScreenWidth/xGrid)
yStep := Ceil(MaxScreenHeight/yGrid)

If Debug
{
	MsgBox, 
	(
		Monitor Array Setup:
		ShortCutDir: %ShortCutDir%
		Number of Monitors: %MonitorCount%
		Screen: %MaxScreenWidth%/%MaxScreenHeight%
		xStep/yStep: %xStep%/%yStep%
	)
	;MsgBox, Screen: %MaxScreenWidth%/%MaxScreenHeight%
	;MsgBox, xStep/yStep: %xStep%/%yStep%
}

;	Layout of Screen for Default Inputs
;---------------------------------------------------------------------

; Wait until Antelope Screens have loaded
	WinWait, "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)", , 5

;	Monitor_1_1(    0, -1080, 1920, 1080)		;[Input 12 - Screen 6] SC3 State of Health (Actually Station View)

; Waveforms - 2 x Western Panels
	Monitor_1_2( 1920, -1080, 1920, 1080)		;[Input 1 - Screen 7]
	Monitor_2_2( 1920,     0, 1920, 1080)		;[Input 2 - Screen 2]

; Waveforms - 2 x Eastern Panels
	Monitor_1_3( 3840, -1080, 1920, 1080)		;[Input 3 - Screen 8] - Waveforms NE (
	Monitor_2_3( 3840,     0, 1920, 1080)		;[Input 4 - Screen 3] - Waveforms SE (Regional)
	Clock_Panel( 3840,     0, 1920, 180)		;[7 - header]

; Antelope Earthquake and Station Views - 2 x Panles
	Monitor_1_4( 5760, -1080, 1920, 1080)		;[Input 5 - Screen 9] Antelope EQ View
	Monitor_2_4( 5760,     0, 1920, 1080)		;[Input 6 - Screen 4] Antelope Station Latency

; SeisComP3 Earthquake and Station Views - 2 x Panels
	Monitor_1_1(    0, -1080, 1920, 1080)		;[Input 12 - Screen 6] SC3 State of Health (Actually Station View)
	Monitor_1_5( 7680, -1080, 1920, 1080)		;[Input 10 - Screen 10] SC3 EQ View

; Temporary Placement Images - For Main Preset
	Monitor_2_5( 7680+480, 270, 960, 540)		;[Input 8 - Screen 5] - EATWS Tier2
	Monitor_2_7( 11520+480, 270, 960, 540)		;[Input 14 - Screen 1] - Nuclear Monitoring Waveforms

;---------------------------------------------------------------------

; Temporary Placement Images - For Other Inputs

	Monitor_1_6(      9600, -1080, 1920, 1080) 		;[Input 13 - Screen 11] - GA Earthquakes Page
	Monitor_1_7( 11520+480, -810, 960, 540) 		;[Input 11 - Screen 13] - SC3 SoH
	Monitor_1_8( 13440+480, -810, 960, 540) 		;[Input 7 - Screen 15] - Tier 1 SoH

	Monitor_2_6( 9600, 0, 960, 540) 			;[Input 9 - Screen 12] - Tier 2 SoH
	Monitor_2_7( 11520+480, 270, 960, 540) 			;[Input 14 - Screen 14] - NucMon Waveforms
	Monitor_2_8( 13440+480, 270, 960, 540) 			;[Input 15 - Screen 16] - NucMon SoH

	MsgBox, EATWS Display Layout Complete


/*	Button Coordinates:	seis-pub.ga.gov.au/widescreen/latency.html
---------------------------------------------------------------------
	Mouse Move Coordinates for Earthquake/Latency Maps - see Monitors 1/3 and 2/1
	Link  := ShortCutDir . "Station_Latency.lnk"
	Title := "seis-pub.ga.gov.au/widescreen/latency.html"

	- Horizontal Grid
	ViewX:		Laptop = 260	TP/PC = 187	EATWS =	214
	BkgX:		Laptop = 490	TP/PC = 354	EATWS =	400
	MapX:		Laptop = 790	TP/PC = 574	EATWS =	640
	- Vertical Grid
	RowY:		Laptop = 37 	TP/PC = 27	EATWS =	70
	Option1Y:	Laptop = 84	TP/PC = 57	EATWS =	106
	Option2Y:	Laptop = 130	TP/PC = 91	EATWS =	141
	Option3Y:	Laptop = 172	TP/PC = 128	EATWS =	178
	Option4Y:	Laptop = 225	TP/PC = 161	EATWS =	215
	Option5Y:	Laptop = 267	TP/PC = 195	EATWS =	255
	- Average Vertical Delta
	RowDY:	Laptop = 46		TP/PC = 34	EATWS =	37
*/

/*	Window Definition Templates
---------------------------------------------------------------------
	;3 Vertical Windows Side by Side
	CMDWindow(x,y+VOffset,width/3,height)
	CMDWindow(x+width/3,y+VOffset,width/3,height)
	CMDWindow(x+(2*width/3),y+VOffset,width/3,height)

	;2 x 2 Grid
	CMDWindow(x,y,width/2,height/2)
	CMDWindow(x+width/2,y,width/2,height/2)
	CMDWindow(x,y+height/2,width/2,height/2)
	CMDWindow(x+width/2,y+height/2,width/2,height/2)

*/

/*	Waveform Display Info
---------------------------------------------------------------------
	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)
	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)

	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)
	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)

	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)
	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)

	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)
	GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)

*/

/*	Useful/Used Links
---------------------------------------------------------------------
	Link := ShortCutDir . "Grafana-Natmap.lnk"
	Title := "Grafana - Natmap"

*/
	
	
CMDWindow(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-CMD] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Run, cmd.exe,,,OutputVarPID
	WinWait, ahk_pid %OutputVarPID%
	xStep := width
	yStep := height
	WinMove, ahk_pid %OutputVarPID%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, ahk_pid %OutputVarPID%
}

LNKWindow(x,y,width,height,LinkName, TitleName)
{	
	If DebugLev2
		MsgBox, 
		(
			[In-LNK] xPos/yPos: %x%/%y%, 
			xStep/yStep: %width%/%height%, 
			Link: %LinkName%, 
			Title: %TitleName%
			ExludeID: %ExcludeID%
		)

	IfWinNotExist, %TitleName%
	{
		Run, %LinkName%
	}

	if %ExcludeID%	;Any Value
	{
		If DebugLev2
			MsgBox, ExcludeID is true = Any Value: %ExcludeID%
		WinWait, %TitleName%,,,ahk_id %ExcludeID%
	} else {
		If DebugLev2
			MsgBox, ExcludeID is False = no value: %ExcludeID%
		WinWait, %TitleName%
	}

	xStep := width
	yStep := height
	WinMove, %TitleName%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, A
}

OpenWindow(x,y,width,height, TitleName)
{	;Activates/Moves Already Open Window

	If DebugLev2
		MsgBox, [In-LNK] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%, Title: %TitleName%
		
	WinActivate, %TitleName%

	xStep := width
	yStep := height
	WinMove, %TitleName%,,%x%,%y%,%xStep%,%yStep%
	GroupAdd, LayoutGroup, A
}


Monitor_1_1(x,y,width,height)		; SC3 Station View
{
	Link  := ShortCutDir . "gempa_stationview.lnk"
	Title := "gempa StationView"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
	;ExcludeID := WindowID		;pass ExcludePID to next interation if needed

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
			ExcludeID: %ExcludeID%
		)

	;Click "Log In" button for EATWS.NET
;	MouseMove, %BtnX%, %BtnYBase%, %Speed%
;	CoordMode, ToolTip|Pixel|Mouse|Caret|Menu [, Screen|Window|Client]
	;CoordMode, Mouse, Client
	MouseMove, 990, 200, 10
	Click, Left
	
}

Monitor_1_2(x,y,width,height)		; Waveforms North West
{	

	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)"

	OpenWindow(x,y+VOffset,width/2,height-Voffset, Title)
	
	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)"
	OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title)
}

Monitor_1_3(x,y,width,height)		; Waveforms South West
{	
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)"

	OpenWindow(x,y+VOffset,width/2,height-Voffset, Title)
	
	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)"
	OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title)

}

Monitor_1_4(x,y,width,height)		; Antelope World Earthquakes
{
	Link  := ShortCutDir . "EQ_Events.lnk"
	;Title := "seis-pub.ga.gov.au/widescreen/latency.html"
	Title := "Integrated Geophysical Networks, Seismic Data Home Page"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
	;ExcludeID := WindowID		;pass ExcludePID to next interation if needed

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 1-4] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
			ExcludeID: %ExcludeID%
		)

	CoordMode, Mouse, Client

	;EATWS x16 Client Coords
	RowY	:= 70
	ViewX	:= 214
	BkgX	:= 400
	MapX	:= 640
	RowDY	:= 37
	MSpeed	:= 10
	
	; Change Map = Earthquake (Opt2)
	If DebugLev2
		MsgBox, SelectButton(%MapX%, %RowY%, %RowDY%, 2, %MSpeed%)
	SelectButton(MapX, RowY, RowDY, 2, MSpeed)

	; Change View = World (Opt5)
	If DebugLev2
		MsgBox, SelectButton(%ViewX%, %RowY%, %RowDY%, 5, %MSpeed%)
	SelectButton(ViewX, RowY, RowDY, 5, MSpeed)	
}

Monitor_1_5(x,y,width,height)		; SC3 EQ Events
{
	Link  := ShortCutDir . "gempa_eqview.lnk"
	Title := "gempa eqview"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
	;ExcludeID := WindowID		;pass ExcludePID to next interation if needed

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
			ExcludeID: %ExcludeID%
		)
}

Monitor_1_6(x,y,width,height)		; Earthquakes @ GA Webpage
{
	Link := ShortCutDir . "Earthquakes_GA.lnk"
	Title := "Geoscience Australia � Earthquakes @ GA"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_1_7(x,y,width,height)		; SC3 Station View (SoH replaced)
{
	Link  := ShortCutDir . "SC3_StationView.lnk"
	Title := "SC3_StationView.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_1_8(x,y,width,height)		; Tier 1 State of Health
{
	Link  := ShortCutDir . "Tier1_SoH.lnk"
	Title := "Tier1_SoH.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_2_1(x,y,width,height)		; SC3 EQ Events
{
	Link  := ShortCutDir . "NucMon_Waveforms.lnk"
	Title := "NucMon_Waveforms.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_2_2(x,y,width,height)		; Waveforms South West
{

	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)"

	OpenWindow(x,y+VOffset,width/2,height-Voffset, Title)

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)"
	OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title)
}

Monitor_2_3(x,y,width,height)		; Waveforms South East
{
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 180

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)"

	OpenWindow(x,y+VOffset,width/2,height-Voffset, Title)

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)"
	OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title)
}

Monitor_2_4(x,y,width,height)	;World Station Latency
{
	Link  := ShortCutDir . "Station_Latency.lnk"
	Title := "rhe-eqm-ops-prod.prod.lan/widescreen/latency.html"

	If DebugLev2
		MsgBox, [IN 2_4] with ExcludeID: %ExcludeID%

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
;	ExcludeID := WindowID		;pass ExcludePID to next interation if needed

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
			ExcludeID: %ExcludeID%
		)

	CoordMode, Mouse, Client

	;EATWS x16 Client Coords - Different to EQ_Events!!
	RowY	:= 30
	ViewX	:= 210
	BkgX	:= 390
	MapX	:= 635
	RowDY	:= 36
	MSpeed	:= 10
	
	; Change View = World (Opt5)
	If DebugLev2
		MsgBox, SelectButton(%ViewX%, %RowY%, %RowDY%, 3, %MSpeed%)
	SelectButton(ViewX, RowY, RowDY, 5, MSpeed)
}

Monitor_2_5(x,y,width,height)		; SC3 EQ Events
{
	Link  := ShortCutDir . "Tier2_Waveforms.lnk"
	Title := "Tier2_Waveforms.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}


Monitor_2_6(x,y,width,height)		; Tier 2 State of Health
{
	Link  := ShortCutDir . "Tier2_SoH.lnk"
	Title := "Tier2_SoH.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_2_7(x,y,width,height)		; NuCMon Waveforms
{
	Link  := ShortCutDir . "NucMon_Waveforms.lnk"
	Title := "NucMon_Waveforms.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Monitor_2_8(x,y,width,height)		; NucMon State of Health
{
	Link  := ShortCutDir . "NucMon_SoH.lnk"
	Title := "NucMon_SoH.JPG - Windows Photo Viewer"

	LNKWindow(x,y,width,height, Link, Title)
	WinGet, WindowID, ID, A
}

Old_Monitor_2_2(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-Monitor 2-2] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	CMDWindow(x,y,width/2,height/2)
	CMDWindow(x+width/2,y,width/2,height/2)
	CMDWindow(x,y+height/2,width,height/2)
}

Clock_Panel(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Link := ShortCutDir . "Clock_Panel.lnk"
	Title := "Clock Panel"
	LNKWindow(x, y, width, height, Link, Title)
}

SelectButton(BtnX,BtnYBase,BtnYStep,BtnOption,Speed)
{
	Option := BtnYBase + ( BtnOption * BtnYStep )
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %BtnYBase%, %Speed%	
	MouseMove, %BtnX%, %BtnYBase%, %Speed%
	Click, Left
	If DebugLev2
		MsgBox, MouseMove, %BtnX%, %Option%, %Speed% 
	MouseMove, %BtnX%, %Option%, %Speed% 
	Click, Left
}