;
; AntelopeGroup_HotKeys.ahk
;
; Description: 	Defines ahk HotKeys to Hide/Show Antelope Group of Windows.
;				
; Requires:		List of Group IDs passed from calling script. As a single string with comma separated ID values
;
; Tony Pack - May 2018
;

global LayoutGroupName := "AntelopeGroup"

	GroupList = %1%
	;MsgBox, GroupList: . %GroupList%

	Loop, parse, GroupList, `,
	{
		;MsgBox, Window number %A_Index% is %A_LoopField%
		GroupAdd, %LayoutGroupName%, ahk_id %A_LoopField%
	}	

^+A::
	if WinActive(ahk_group %LayoutGroupName%)
	{
		MsgBox % LayoutGroupName . " is Active - so Hide"
		WinHide, ahk_group %LayoutGroupName%
	} else {
		MsgBox % LayoutGroupName . " is NotActive - so Show"
		WinShow, ahk_group %LayoutGroupName%
	}
	Return
	
^+T::
	WinHide, ahk_group %LayoutGroupName%
	Return
	
^+Y::
	WinShow, ahk_group %LayoutGroupName%
	Return

^+U::
	WinClose, ahk_group %LayoutGroupName%
	ExitApp
	Return