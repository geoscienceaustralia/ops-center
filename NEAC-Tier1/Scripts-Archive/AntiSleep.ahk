;
; AntiSleep.ahk
;

/*	
---------------------------------------------------------------------
	EATWS 16 Output PC is configured as 16x 1920/1080 monitors
	Horizontal = 15360 by Vertical = 2160
*/

CoordMode, Mouse, Screen

Loop {
	if (A_TimeIdle > 10000) {
		; don't do anything unless no user input for at least 10 seconds
		MouseGetPos, xpos, ypos 
		MouseMove, xpos, ypos
		MouseMove, xpos+100, ypos+100, 10
		MouseMove, xpos, ypos
		Sleep, 600000	; Sleep for 10 minutes: 1000 msec = 1 second
	}
}

