; Script Test

; 	WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
;	WinWait, Terminal
;	WinActivate

	FormatTime, Time,, MM/dd/yy hh:mm tt 

;	ControlSend, , echo CLOCK 1 The current time is: %Time% {enter}
;	ControlSend, , notepad.exe {enter}
;	ControlSend, , ./eaca.sh {enter}
	
	; run, clock.bat %A_Index% "%WinTitle%"
	MobaXterm_cmd := "C:\LocalInstall\MobaXterm_Portable_v10.2\MobaXterm_Personal_10.2.exe" 
	
	run %MobaXterm_cmd% -bookmark "Proc1Stack1-EACA"
	
	WinWait, scrttv@localhost@proc1
	WinActivate
	WinGet, OutputVar , PID, scrttv@localhost@proc1

	ControlSend, , > {enter}	

	; WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, ahk_pid  %OutputVar%, ,2390, -1080, 960, 2160
	Sleep 1000
	
	; GroupAdd, GroupName , WinTitle, WinText, Label, ExcludeTitle, ExcludeText
	GroupAdd, ScriptTest, ahk_pid %OutputVar%
	WinHide, ahk_group ScriptTest

	MsgBox, Ok to Continue
	
;ExitApp
;	===================================================	
	
	MobaXterm_cmd := "C:\LocalInstall\MobaXterm_Portable_v10.2\MobaXterm_Personal_10.2.exe" 
	
	run %MobaXterm_cmd% -bookmark "Proc1Stack1-NSA"
	
	WinWait, scrttv@localhost@proc1
	WinActivate
	WinGet, OutputVar , PID, scrttv@localhost@proc1

	ControlSend, , > {enter}	

	; WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, ahk_pid  %OutputVar%, ,3400, -1080, 960, 2160
	Sleep 1000
	
	; GroupAdd, GroupName , WinTitle, WinText, Label, ExcludeTitle, ExcludeText
	GroupAdd, ScriptTest, ahk_pid %OutputVar%

;	WinHide, ahk_group ScriptTest
	
	WinShow, ahk_group ScriptTest
	
	MsgBox, Ok to Continue

^+F::
	WinHide, ahk_group ScriptTest
	Return
	
^+G::
	WinShow, ahk_group ScriptTest
/*	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	Loop % GroupList
	{
		WinShow , ahk_id GroupList%A_Index%
		WinActivate , ahk_id GroupList%A_Index%
		; MsgBox % "Window: " . %A_Index% . " - " . GroupList%A_Index%
	}
*/
	return

^+H::
	WinClose, ahk_group ScriptTest
	ExitApp
	Return	
	
/*
	Run, clock.bat %ID%,,,OutputVarPID
;	WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	WinWait, ahk_pid %OutputVarPID%, , 1, ahk_group %LayoutGroupName%
	WinMove, ahk_pid %OutputVarPID%,,%x%,%y%,%width%,%height%
;	GroupAdd, GroupName , WinTitle, WinText, Label, ExcludeTitle, ExcludeText
	GroupAdd, %LayoutGroupName%, ahk_pid %OutputVarPID%	
	
*/