;	SeisComP3 - Waveform Setup
;	===================================================	
global MobaXterm_cmd := "C:\LocalInstall\MobaXterm_Portable_v10.2\MobaXterm_Personal_10.2.exe" 

	;	Start MobaXterm and open connection to Proc1 (AWS)
	run %MobaXterm_cmd% -bookmark SC3connectProc1 	;-hideterm  -exitwhendone
	; DEF: WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	WinWait, SC3connectProc1
	Sleep 2000
	;	Run script to open all 5 SC3 Windows
	run %MobaXterm_cmd% -bookmark SC3Proc1scrttv -hideterm  -exitwhendone
