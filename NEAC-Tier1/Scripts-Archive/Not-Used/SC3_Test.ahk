; 	Script Test
;	===================================================	

#SingleInstance force		; If a previous instance of script is running - forces the replacement of the old instance with the new instance.

SetTitleMatchMode, 2		; Title Match Mode = can contain text

global TestMode := True
global DebugInfo := False
global CMDWaitTime := 5000
global xBase := -1900

global ExcludeTitle

global MobaXterm_cmd := "C:\LocalInstall\MobaXterm_Portable_v10.2\MobaXterm_Personal_10.2.exe" 

;	===================================================	

;	DEF: Progress, ProgressParam1 , SubText, MainText, WinTitle, FontName
	Progress, B1 X150 Y150 ZX20 ZY20, Loading Stack 1, Loading EATWS Opperations Room Screen Layout, My Title
	Progress, 5 	; Set the position of the bar to x%.

;	MsgBox % "TestMode: = " . TestMode

;	#1 - EACA
;	===================================================	
	
	if !TestMode
	{
		; MsgBox % "Not TestMode"
		Stack := "Proc1Stack1-EACA"
		xPos := 1920
		yPos := -1080
		Width := 960
		Height := 2160
	} else {
		MsgBox % "TestMode"
		Stack := 1
		xPos := xBase + 1920
		yPos := 0
		Width := 480
		Height := 960
	}
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	Progress, 25, Loading Stack 2
	
;	#2 - AIWA
;	===================================================	
	
	if !TestMode
	{
		Stack := "Proc1Stack1-AIWA"
		xPos := 2880
		yPos := -1080
		Width := 960
		Height := 2160
	} else {
		Stack := 2
		xPos := xBase + 2400
		yPos := 0
		Width := 480
		Height := 960
	}
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	Progress, 50, Loading Stack 3
	
;	#3 - EPEA
;	===================================================	

	if !TestMode
	{
		Stack := "Proc1Stack1-EPEA"
		xPos := 3840
		yPos := -1080
		Width := 960
		Height := 2160
	} else {
		Stack := 3
		xPos := xBase + 2880
		yPos := 0
		Width := 480
		Height := 960
	}
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	Progress, 75, Loading Stack 4

;	#4 - NSA
;	===================================================	

	if !TestMode
	{
		Stack := "Proc1Stack1-NSA"
		xPos := 4800
		yPos := -1080
		Width := 960
		Height := 2160
	} else {
		Stack := 4
		xPos := xBase + 3360
		yPos := 0
		Width := 480
		Height := 960
	}
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	Progress, 95, Completing Screen Layout

;	===================================================	
	
	Progress, 100
	Sleep 4000
;	MsgBox, Ok to Continue
	Progress, Off
	
;	===================================================	

;ExitApp

^+F::
	WinHide, ahk_group ScriptTest
	Return
	
^+G::
	WinShow, ahk_group ScriptTest
/*	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	Loop % GroupList
	{
		WinShow , ahk_id GroupList%A_Index%
		WinActivate , ahk_id GroupList%A_Index%
		; MsgBox % "Window: " . %A_Index% . " - " . GroupList%A_Index%
	}
*/
	return

^+H::
	WinClose, ahk_group ScriptTest
	ExitApp
	Return	
	
	
Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
{
;	if DebugInfo
;		MsgBox % "Load_SC3Waveforms: " Stack "," xPos "," yPos "," Width "," Height " Existing Exclude: [" ExcludeTitle "]"

	if !TestMode
	{
		run %MobaXterm_cmd% -bookmark %Stack%
	} else {
		run clock.bat %Stack% scrttv@localhost@proc1
	}
	Sleep %CMDWaitTime%

	; DEF: WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	if ExcludeText = ""			; ExcludeText not set yet
	{
		if DebugInfo
			MsgBox % "Existing ExcludeText (should be blank): [" ExcludeTitle "]"
		WinWait, scrttv@localhost
	} else {					; ExcludeText set already
		if DebugInfo
			MsgBox % "Existing ExcludeText: [" ExcludeTitle "]"
		WinWait, scrttv@localhost, , , %ExcludeTitle%	
	}
	
;	WinWait, scrttv@localhost, , , %ExcludeTitle%
;	WinWait, scrttv@localhost, , , ahk_group ScriptTest
	
	; DEF: WinGet, OutputVar , Cmd, WinTitle, WinText, ExcludeTitle, ExcludeText
	;WinGet, OutputVar, ID, scrttv@localhost, , %ExcludeTitle%

	WinGet, OutputVar, PID				; use window returned from WinWait
	if DebugInfo
		MsgBox % "Found Window PID: " OutputVar
	
	;ExcludeTitle	:= ExcludeTitle . "ahk_id " . OutputVar . " "
	ExcludeTitle	:= LTrim(ExcludeTitle) . "ahk_pid " . OutputVar . " "
		
	if DebugInfo
		MsgBox % "New Exclude Title Text: [" ExcludeTitle "]"

	;if !TestMode
	;	ControlSend, , > {enter}	; Zoom in one step...

	; DEF: WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, ahk_pid  %OutputVar%, ,%xPos%, %yPos%, %Width%, %Height%, %ExcludeTitle%
	GroupAdd, ScriptTest, ahk_pid %OutputVar%

;	Sleep %CMDWaitTime%
	
}	