; 	Remove SCRTTV Windows Test
;	===================================================	

	SetTitleMatchMode, 2
	
	; DEF: WinGet, OutputVar , Cmd, WinTitle, WinText, ExcludeTitle, ExcludeText
	WinGet, SC3WinList , List, scrttv@localhost
	
	Loop, %SC3WinList% 
	{
		; will run loop for number of windows in array
		WinActivate, % "ahk_id " SC3WinList%A_Index%
		MsgBox, This is window %A_Index% in the list.
		; 	DEF: WinClose , WinTitle, WinText, SecondsToWait, ExcludeTitle, ExcludeText
		WinClose, % "ahk_id " SC3WinList%A_Index%
	}

	