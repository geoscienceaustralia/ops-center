;
; 	PC Main: VideoWall Screens Layout - SeiscomP3 Configuration
;
; 	Description: Positions SeiscomP3 Waveform, Events and Station Latency views 
; 	along with Title Screens etc. on Startup and as requested through desktop shortcut
;
; 	Tony Pack - June 2018
;	===========================================================================


SetTitleMatchMode, 2		; Title Match Mode = can contain text
#SingleInstance force		; Replaces any old instance of scripts automatically

global Debug := False
global DebugLev2 := False
global Mode := "Local"			; "Test" for 2 Screen Test Environment: "Local" for EATWS VideoWall
global WinWaitTime := 2
global LayoutGroupName := "SC3_Group"

global ShortCutDir := A_Desktop . "\"

SetTitleMatchMode, 2		; Title Match Mode = can contain text

global TestMode := False
global DebugInfo := False
global CMDWaitTime := 20000

global ExcludeTitle

global MobaXterm_cmd := "C:\LocalInstall\MobaXterm_Portable_v10.2\MobaXterm_Personal_10.2.exe" 

;	Setup Progress Screen
;	===========================================================================
;	DEF: Progress, ProgressParam1 , SubText, MainText, WinTitle, FontName
;	Progress, B1 X4700 Y-400 ZX20 ZY20, Initialising, Loading EATWS Operations Room Screen Layout, My Title
	Progress, B1 ZX20 ZY20, Initialising, Loading EATWS Operations Room Screen Layout, My Title
	Progress, 0 	; Set the position of the bar to x%.


;	Get Video Wall dimensions - Set Test/Live Mode
;	===========================================================================
;	EATWS Video Wall = 8 x 2 = 16 Monitors
;		Width = 8 x 1920 = 15360
;		Height = 2 x 1080 = 2160
;		New Height = 3 x 1080 = 1080 to -2160
;
;	2 Screen Test Mode = 2 Monitors
;		Width = 2 x 1920 = 3840
;		Height = 1080

	SysGet, VirtualScreenWidth, 78
	SysGet, VirtualScreenHeight, 79
	MaxScreenWidth := VirtualScreenWidth
	MaxScreenHeight := VirtualScreenHeight

	if (MaxScreenWidth = 3840)
		Mode := "Test"
	
	If DebugLev2
	{
		MsgBox % "MaxScreenWidth: " . MaxScreenWidth
		MsgBox % "Mode: " . Mode
		MsgBox % "ShortCutDir: " . ShortCutDir
	}
	
;	Check if existing HotKeys Group is running and stop if is:
;	===========================================================================
;	Stop_ExistingAntelopeGroup_HotKeys() - Not Working

;	Load Arrays:
;	===========================================================================
	global WinCoords := []		; Size and Coordinates for Windows
	Load_WinCoords(Mode)
	
	If DebugLev2
		MsgBox % "Width/Height: " . WinCoords[0].width . "/" .  WinCoords[0].height

	If DebugLev2
	{	
		loop % 16	; 16 Windows
			MsgBox % "WinCords[" . A_Index . "]x/y: " . WinCoords[A_Index].x . "/" .  WinCoords[A_Index].y
	}


;	Load Webpages:
;	===========================================================================
	Progress, 5, Clock Panel
	Sleep 1000	
	Clock_Panel( WinCoords[6].x, WinCoords[6].y, WinCoords[0].width, 150)	; [Screen 6 - Top Left - head]

	Progress, 10, Earthquakes@GA
	Sleep 1000	
	EarthquakesAtGA( WinCoords[10].x, WinCoords[10].y, WinCoords[0].width, WinCoords[0].height)	; [Screen 10 - Top Right]

; SeisComP3 Earthquake and Station Views - 2 x Panels
	Progress, 15, SeisComP3 Station View
	Sleep 1000	
	SC3_Stationview( WinCoords[9].x, WinCoords[9].y, WinCoords[0].width, WinCoords[0].height)		;[Input 12 - Screen 6] SC3 Station View

	Progress, 20, SeisComP3 Events View
	Sleep 1000	
	SC3_EQview(      WinCoords[4].x, WinCoords[4].y, WinCoords[0].width, WinCoords[0].height)		;[Input 10 - Screen 10] SC3 EQ View

	Progress, 25, SeisComP3 State of Health
	Sleep 1000	
	SC3_SoH(     	 WinCoords[14].x, WinCoords[14].y, WinCoords[0].width, WinCoords[0].height)		;[Input 10 - Screen 10] SC3 EQ View
	
;	================================================================= SC3 Waveforms - START ================================================

;	Set AutohotKey Cmd for Test vs Normal operation
;	===================================================		
; 	MobaXterm SC3 windows not recognised using Process ID (PID)
;	Need to use Unique ID (ID) in stead.

	global AHK_Cmd := "ID"
	global AHK_TitleType := "ahk_id"

;	Close any existing scrttv views
;	===================================================		
	Progress, 30, Close Existing SeisComP3 Waveforms windows
	Sleep 1000
	WinGet, SC3WinList , List, scrttv@localhost
	
	Loop, %SC3WinList% 
	{
		WinActivate, % "ahk_id " SC3WinList%A_Index%
		; MsgBox, This is window %A_Index% in the list.
		WinClose, % "ahk_id " SC3WinList%A_Index%
	}

	
;	#1 - EACA
;	===================================================	
	Stack := "Proc1Stack1-EACA"
	Progress, 40, SeisComP3 Waveforms - EACA
	
	xPos := WinCoords[7].x
	yPos := WinCoords[7].y
	Width := WinCoords[0].width / 2
	Height := WinCoords[0].height * 2
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	
;	#2 - AIWA
;	===================================================	
	Stack := "Proc1Stack1-AIWA"
	Progress, 50, SeisComP3 Waveforms - AIWA

	xPos := WinCoords[7].x + (WinCoords[0].width / 2)
	yPos := WinCoords[7].y
	Width := WinCoords[0].width / 2
	Height := WinCoords[0].height * 2
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
	
;	#3 - EPEA
;	===================================================	
	Stack := "Proc1Stack1-EPEA"
	Progress, 60, SeisComP3 Waveforms - EPEA

	xPos := WinCoords[8].x
	yPos := WinCoords[8].y
	Width := WinCoords[0].width / 2
	Height := WinCoords[0].height * 2
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)

;	#4 - NSA
;	===================================================	
	Stack := "Proc1Stack1-NSA"
	Progress, 70, SeisComP3 Waveforms - NSA
	
	xPos := WinCoords[8].x + (WinCoords[0].width / 2)
	yPos := WinCoords[8].y
	Width := WinCoords[0].width / 2
	Height := WinCoords[0].height * 2
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)

;	#5 - SCRTTV
;	===================================================	
	Stack := "Proc1Stack1-scrttv"
	Progress, 80, SeisComP3 Waveforms - Event Stack
	
	xPos := WinCoords[6].x
	yPos := WinCoords[6].y + 140
	Width := WinCoords[0].width
	Height := WinCoords[0].height - 140
	Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)

;	================================================================== SC3 Waveforms - END =================================================

;	MsgBox, If an alert window appears - Please Click "Yes" to replace existing version and continue
	Call_SC3GroupHotKeys()

;	Reposition Mouse Cursor on Home Screen
;	DllCall("SetCursorPos", int, 960, int, 480)

	Progress, 100, EATWS Display Layout Complete
	Sleep 10000
	Progress, Off

ExitApp

;	Functions:
;---------------------------------------------------------------------
;	Library
	#Include ./Lib/Display.ahk

Load_SC3Waveforms(Stack, xPos, yPos, Width, Height)
{
;	global AHK_Cmd := "PID" for TestMode or "ID" for Normal
;	global AHK_TitleType := "ahk_pid" for TestMode or "ahk_id" for Normal

	run %MobaXterm_cmd% -bookmark %Stack% -hideterm  -exitwhendone
	Sleep %CMDWaitTime%
			
	; DEF: WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	if ExcludeText = ""			; ExcludeText not set yet
	{
		if DebugInfo
			MsgBox % "Existing ExcludeText (should be blank): [" ExcludeTitle "]"
		WinWait, scrttv@localhost
	} else {					; ExcludeText set already
		if DebugInfo
			MsgBox % "Existing ExcludeText: [" ExcludeTitle "]"
		WinWait, scrttv@localhost, , , %ExcludeTitle%	
	}
	
	; DEF: WinGet, OutputVar , Cmd, WinTitle, WinText, ExcludeTitle, ExcludeText
	WinGet, OutputVar, %AHK_Cmd%	; use window returned from WinWait

	if DebugInfo
		MsgBox % "Found Window " AHK_Cmd ": " OutputVar
	
	ExcludeTitle	:= LTrim(ExcludeTitle) . AHK_TitleType . " " . OutputVar . " "
		
	if DebugInfo
		MsgBox % "New Exclude Title Text: [" ExcludeTitle "]"

;	if (Stack = "Proc1Stack1-EACA")
	ControlSend, , > {enter}	; Zoom in one step...

	; DEF: WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, %AHK_TitleType%  %OutputVar%, ,%xPos%, %yPos%, %Width%, %Height%, %ExcludeTitle%
	GroupAdd, %LayoutGroupName%, %AHK_TitleType% %OutputVar%
}	
	
SC3_Stationview(x,y,width,height)		; SC3 Station View
{
	Link  := ShortCutDir . "gempa_stationview.lnk"
	Title := "gempa StationView"
	
	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A

;	Click "Log In" button for EATWS.NET
;	MouseMove, %BtnX%, %BtnYBase%, %Speed%
;	DEF: CoordMode, ToolTip|Pixel|Mouse|Caret|Menu [, Screen|Window|Client]
	WinActivate
	CoordMode, Mouse, Client
	MouseMove, 1000, 195, 10
	Click, Left	
}

SC3_EQview(x,y,width,height)			; SC3 EQ Events
{
	Link  := ShortCutDir . "gempa_eqview.lnk"
	Title := "gempa eqview"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A
}

SC3_EQview_Local(x,y,width,height)		; SC3 EQ Events - Localised View
{
	Link  := ShortCutDir . "gempa_eqview.lnk"
	Title := "gempa eqview"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A
}

SC3_SoH(x,y,width,height)				; SC3 State of Health
{
	Link  := ShortCutDir . "gempa State of Health.lnk"
	Title := "Check_MK"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A

;	Click "Log In" button for EATWS.NET
;	DEF: CoordMode, ToolTip|Pixel|Mouse|Caret|Menu [, Screen|Window|Client]
	WinActivate
	CoordMode, Mouse, Client
	MouseMove, 1110, 580, 10
	Click, Left	
}

EarthquakesAtGA(x,y,width,height)		; Earthquakes @ GA Webpage
{
	Link := ShortCutDir . "EQ@GA Web.lnk"
	Title := "Earthquakes@GA"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A
}

Clock_Panel(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Link := ShortCutDir . "Clock_Panel.lnk"
	Title := "Clock Panel"
	Display_LNKWindow(x, y, width, height, Link, Title, LayoutGroupName)
}

Load_WinCoords(Mode)
{
;	Screen/Window Coordinates:
;	===========================================================================
;	WinCoords := []		initiate the array object in Head before calling function
;	WinCoords [ScreenNo].x & [ScreenNo].y = Top Left Coordinate of Window
;	WinCoords[0].width 	= Width 
;	WinCoords[0].height	= Height
;	
;	EATWS Video Wall = 8 x 2 = 16 Monitors
;		Width = 8 x 1920 = 15360
;		Height = 2 x 1080 = 2160
;
;	2 Screen Test Mode = 2 Monitors
;		Width = 2 x 1920 = 3840
;		Height = 1080

	If DebugLev2
		MsgBox % "In Load_WinCoords with Mode = " . Mode

	if (Mode = "Local")
	{
		StepX := 1920
		StepY := 1080
		BaseY := -1080	; was 0
		TopY  := -1080
	}
	else if (Mode = "Test")
	{	; for dual monitors a suitable test mode is 640 x 540
		StepX := 680
		StepY := 540
		BaseY := 540
		TopY  := 0
	}
	
	If DebugLev2
		MsgBox % "Load_WinCoords: StepX,StepY,BaseY,TopY: " . StepX . "," . StepY . "," . BaseY . "," . TopY
			
	;Screens 	Width and Height
	;-------------------------------------------------
	WinCoords[0] := {width: StepX, height: StepY}

	;Bottom VideoWall Screens 	1 to 5 (Left to Right)
	;-------------------------------------------------
	WinCoords[1] 	:= {x: StepX * 0, y: BaseY}	; Bottom Left: Y = 0 or 540
	WinCoords[2] 	:= {x: StepX * 1, y: BaseY}
	WinCoords[3] 	:= {x: StepX * 2, y: BaseY}
	WinCoords[4] 	:= {x: StepX * 3, y: BaseY}
	WinCoords[5] 	:= {x: StepX * 4, y: BaseY}	; Bottom Right - Placements X+480, Y=270

	; Top VideoWall Screens 		6 to 10 (Left to Right)
	; --------------------------------------------------
	; Top of Bottom Screen = 0 / Top of Top Screen = -1080
	WinCoords[6] 	:= {x: StepX * 0, y: TopY}	; Top Left: Y = -1080 or 0
	WinCoords[7] 	:= {x: StepX * 1, y: TopY}
	WinCoords[8] 	:= {x: StepX * 2, y: TopY}
	WinCoords[9] 	:= {x: StepX * 3, y: TopY}
	WinCoords[10] 	:= {x: StepX * 4, y: TopY}	; Top Right

	;Non-Visible Screens 11 to 16
	;--------------------------------------------
	; Third In from Far Right	11=Top/12=Bottom
	WinCoords[11] 	:= {x: StepX * 5, y: TopY}
	WinCoords[12] 	:= {x: StepX * 5, y: BaseY}
	;--------------------------------------------
	; Second In from Far Right	13=Top/14=Bottom
	WinCoords[13] 	:= {x: StepX * 6, y: TopY}
	WinCoords[14] 	:= {x: StepX * 6, y: BaseY}
	;--------------------------------------------
	;Far Right					15=Top/16=Bottom
	WinCoords[15] 	:= {x: StepX * 7, y: TopY}
	WinCoords[16] 	:= {x: StepX * 7, y: BaseY}
	;--------------------------------------------
	
	Return 	; WinCoords[]
}

Load_AntelopeTitles()
{
;	Antelope Window Titles:
;	===========================================================================
;	AntelopeTitle := [] 	Initiate the array object in Head before calling function

	If DebugLev2
		MsgBox, Loading Antelope Window Titles

	AntelopeTitle[1] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)"
	AntelopeTitle[2] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)"
	AntelopeTitle[3] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)"
	AntelopeTitle[4] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)"
	AntelopeTitle[5] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)"
	AntelopeTitle[6] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)"
	AntelopeTitle[7] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)"
	AntelopeTitle[8] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)"
	Return AntelopeTitle[]
}

Load_AntelopeWindows(GroupName)
{
	If DebugLev2
		MsgBox, Loading Windows with Mode = SingleScreenTest

	AlertLink := ShortCutDir . "Alert1.lnk"
	
	Loop, % AntelopeTitle.MaxIndex()
	{
		WinTitle 	= % AntelopeTitle[A_Index]
		WinCoordX 	= % WinCoords[A_Index].x		; Not Final Position - but highlights that window exists
		WinCoordY 	= % WinCoords[A_Index].y		; Not Final Position - but highlights that window exists
		WinSizeW	= % WinCoords[0].width
		WinSizeH 	= % WinCoords[0].height
		
		If DebugLev2
			MsgBox % A_Index . " = " . WinTitle . ": " . WinCoordX . "/" . WinCoordY . "/" . WinSizeW . "/" . WinSizeH
			
		If !WinExist(WinTitle)
		{
			; TODO: Presume that non of the Antelope windows exist - fix this presumption later
			; run, clock.bat %A_Index% "%WinTitle%"
			run, %AlertLink%
			sleep 20000			; wait 20 seconds... for windows to open.
			
			; Add Putty Window to Group as well
			GroupAdd, %GroupName%, rhe-eqm-alert-prod1.prod.ext - Putty

			If DebugLev2
				MsgBox %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%	
				
			WinWait, %WinTitle%
		}
		Else	; Window Exists
		{
			If DebugLev2
				MsgBox, Window "%WinTitle%" Exists
		}
		
		If WinExist(WinTitle)
		{
			GroupAdd, %GroupName%, %WinTitle%
			; WinMove, %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%
		}
	}
}

Call_SC3GroupHotKeys()
{
	;	Get a list of Window IDs from ahk_group and pass to HotKey Script as single string:
	;	===================================================================================

	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	GroupListText := ""
	Loop % GroupList
	{
		;MsgBox % GroupList%A_Index%

		;	GroupListText := GroupListText . GroupList%A_Index%
		temp := GroupList%A_Index%
		if (A_Index=1)
			GroupListText = %temp%
		else	
			GroupListText = %GroupListText%,%temp%
		
		;MsgBox, % GroupListText
	}
	If DebugLev2
		MsgBox, GroupListText: %GroupListText%
		
	Run SC3Group_HotKeys.exe %GroupListText%
	return
}

Stop_ExistingAntelopeGroup_HotKeys()
{
	DetectHiddenWindows, On
	SetTitleMatchMode, 2
	ProcessName = "AntelopeGroup_HotKeys.exe"
	If WinExist(ProcessName)
	{
		If DebugLev2
			MsgBox, ProcessName: %ProcessName% Running - so Close
		Process, Close , %ProcessName%
	} Else {
		If DebugLev2
			MsgBox, Process not found
	}
	DetectHiddenWindows, Off
	Return
}