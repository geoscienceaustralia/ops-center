; Script Test

	; WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	WinWait, Terminal
	WinActivate

	FormatTime, Time,, MM/dd/yy hh:mm tt 

	ControlSend, , echo CLOCK 1 The current time is: %Time% {enter}
	ControlSend, , notepad.exe {enter}
	
	; WinWait, ahk_pid %OutputVarPID%, , 1, ahk_group %LayoutGroupName%
	WinWait, Untitled - Notepad
	WinActivate
	ControlSend, , Notepad 1 {enter}
		
	; WinGet, OutputVar , Cmd, WinTitle, WinText, ExcludeTitle, ExcludeText
	WinGet, OutputVar , PID, A
	ControlSend, , %OutputVar% {enter}	

	; WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, ahk_pid  %OutputVar%, ,100, 100, 200, 400
	
	; GroupAdd, GroupName , WinTitle, WinText, Label, ExcludeTitle, ExcludeText
	GroupAdd, ScriptTest, ahk_pid %OutputVar%
	WinHide, ahk_group ScriptTest

	
;	MsgBox, Ok to Continue
;	===================================================	
	
	WinWait, Terminal
	
	ControlSend, , echo CLOCK 2 The current time is: %Time% {enter}
	ControlSend, , notepad.exe {enter}
	
	WinWait, Untitled - Notepad, , , ahk_group ScriptTest
	WinActivate
	ControlSend, , Notepad 2 {enter}
		
	; WinGet, OutputVar , Cmd, WinTitle, WinText, ExcludeTitle, ExcludeText
	WinGet, OutputVar , PID, A
	ControlSend, , %OutputVar% {enter}	

	; WinMove, WinTitle, WinText, X, Y , Width, Height, ExcludeTitle, ExcludeText
	WinMove, ahk_pid  %OutputVar%, ,300, 100, 200, 400
	GroupAdd, ScriptTest, ahk_pid %OutputVar%
	WinShow, ahk_group ScriptTest
	
	MsgBox, Ok to Continue


^+Q::
	WinHide, ahk_group ScriptTest
	Return
	
^+W::
	WinShow, ahk_group ScriptTest
/*	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	Loop % GroupList
	{
		WinShow , ahk_id GroupList%A_Index%
		WinActivate , ahk_id GroupList%A_Index%
		; MsgBox % "Window: " . %A_Index% . " - " . GroupList%A_Index%
	}
*/
	return

^+E::
	WinClose, ahk_group ScriptTest
	ExitApp
	Return	
	
/*
	Run, clock.bat %ID%,,,OutputVarPID
;	WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
	WinWait, ahk_pid %OutputVarPID%, , 1, ahk_group %LayoutGroupName%
	WinMove, ahk_pid %OutputVarPID%,,%x%,%y%,%width%,%height%
;	GroupAdd, GroupName , WinTitle, WinText, Label, ExcludeTitle, ExcludeText
	GroupAdd, %LayoutGroupName%, ahk_pid %OutputVarPID%	
	
*/