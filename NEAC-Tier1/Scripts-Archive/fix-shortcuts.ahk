;
; Fix Windows Shortcuts to run without security warning
;

/*
Example Security Level listing
==============================
** Original - as created by "save to desktop" from Chrome **
Asks for confirmation "OK" when double clicked from Desktop
gempa_StationView.lnk NT AUTHORITY\SYSTEM:(F)
                      BUILTIN\Administrators:(F)
                      PROD\u69679:(F)
                      Mandatory Label\Low Mandatory Level:(I)(NW)

** As Revised by ICACLS command below: **
Does NOT ask for confirmation "OK" when double clicked from Desktop
gempa_StationView2.lnk NT AUTHORITY\SYSTEM:(F)
                       BUILTIN\Administrators:(F)
                       PROD\u69679:(F)
                       NT AUTHORITY\SYSTEM:(I)(OI)(CI)(F)
                       BUILTIN\Administrators:(I)(OI)(CI)(F)
                       PROD\u69679:(I)(OI)(CI)(F)
                       Mandatory Label\Medium Mandatory Level:(NW)
*/

SetWorkingDir, A_Desktop
;MsgBox, A_Desktop: %A_Desktop%

RUN ICACLS *.LNK /L /T /SETINTEGRITYLEVEL M
