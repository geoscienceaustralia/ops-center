;
; PC Main: VideoWall Screens Layout - Antelope Only Open and Layout
;
; Description: Positions Antelope Waveform, Events and Station Latency views 
; along with Title Screens and SeisComP3 screens on Startup
;
; Tony Pack - January 2018
;

SetTitleMatchMode, 2		; Title Match Mode = can contain text

global Debug := False
global DebugLev2 := False
global Mode := "Local"			; "Test" for 2 Screen Test Environment: "Local" for EATWS VideoWall
global WinWaitTime := 2
global LayoutGroupName := "AntelopeGroup"

;global ShortCutDir := "C:\Users\tonyp\Desktop\"
;global ShortCutDir := "C:\Users\u69679\Desktop\"
;global ShortCutDir := "C:\Users\jatwc-new\Desktop\"
global ShortCutDir := A_Desktop . "\"

SysGet, VirtualScreenWidth, 78
SysGet, VirtualScreenHeight, 79
MaxScreenWidth := VirtualScreenWidth
MaxScreenHeight := VirtualScreenHeight
;	EATWS Video Wall = 8 x 2 = 16 Monitors
;		Width = 8 x 1920 = 15360
;		Height = 2 x 1080 = 2160
;
;	2 Screen Test Mode = 2 Monitors
;		Width = 2 x 1920 = 3840
;		Height = 1080


	if (MaxScreenWidth = 3840)
	{
		Mode := "Test"
	}
	
	If DebugLev2
	{
		MsgBox % "MaxScreenWidth: " . MaxScreenWidth
		MsgBox % "Mode: " . Mode
		MsgBox % "ShortCutDir: " . ShortCutDir
	}
		
;ExitApp

;	Check if existing HotKeys Group is running and stop if is:
;	===========================================================================
;	Stop_ExistingAntelopeGroup_HotKeys() - Not Working

;	Load Arrays:
;	===========================================================================
	global WinCoords := []		; Size and Coordinates for Windows
	Load_WinCoords(Mode)
	
	If DebugLev2
		MsgBox % "Width/Height: " . WinCoords[0].width . "/" .  WinCoords[0].height

	If DebugLev2
	{	
		loop % 16	; 16 Windows
			MsgBox % "WinCords[" . A_Index . "]x/y: " . WinCoords[A_Index].x . "/" .  WinCoords[A_Index].y
	}
	
	global AntelopeTitle := [] 	; Titles for Antelope Windows 1 to 8
	Load_AntelopeTitles()

	
;	Check Antelope Windows Exist or Load:
;	===========================================================================

;	if (Mode = "Test") 		; Load Single Screen Test layout
;	{
		Load_ScreenTest(LayoutGroupName)
;	}

	MsgBox	; OK to Continue?

	;	Load Antelope Windows:
	;	===========================================================================
	;	AntelopeWindow(Index,WinTitle,WinCoordX,WinCoordY,WinSizeW,WinSizeH)

		
	; Wait until Antelope Screens have loaded
	;	WinWait, "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)", , 5

	; Antelope Waveforms - 2 x Western Panels
		Waveforms_NW( WinCoords[7].x, WinCoords[7].y, WinCoords[0].width, WinCoords[0].height)		; [Input 1 - Screen 7]
		Waveforms_SW( WinCoords[2].x, WinCoords[2].y, WinCoords[0].width, WinCoords[0].height)		; [Input 2 - Screen 2]

	; Antelope Waveforms - 2 x Eastern Panels
		Waveforms_NE( WinCoords[8].x, WinCoords[8].y, WinCoords[0].width, WinCoords[0].height)		; [Input 3 - Screen 8] - Waveforms NE
		Waveforms_SE( WinCoords[3].x, WinCoords[3].y, WinCoords[0].width, WinCoords[0].height)		; [Input 4 - Screen 3] - Waveforms SE (Regional)
		Clock_Panel( WinCoords[3].x, WinCoords[3].y, WinCoords[0].width, 150)					; [Screen 3 - header] - Waveforms SE (Regional)

	; Antelope Antelope Earthquake and Station Views - 2 x Panles
		Antelope_Earthquakes( WinCoords[9].x, WinCoords[9].y, WinCoords[0].width, WinCoords[0].height)	;[Input 5 - Screen 9] Antelope EQ View
		Antelope_Stations( WinCoords[4].x, WinCoords[4].y, WinCoords[0].width, WinCoords[0].height)		;[Input 6 - Screen 4] Antelope Station Latency

	;---------------------------------------------------------------------


	MsgBox, If an alert window appears - Please Click "Yes" to replace existing version and continue
	Call_AntelopeGroupHotKeys()

;	Reposition Mouse Cursor on Home Screen
	; Keypad 1 - PC Screen 1 - Input 16 - 0/0
;	DllCall("SetCursorPos", int, 100, int, 100)
	DllCall("SetCursorPos", int, 960, int, 480)
;	SendCtrl()

	MsgBox, EATWS Display Layout Complete

ExitApp

;---------------------------------------------------------------------
;	Library
#Include ./Lib/Display.ahk

AntelopeWindow(Screen,WinTitle,WinCoordX,WinCoordY,WinSizeW,WinSizeH,GroupName)
{
	If DebugLev2
		MsgBox, AntelopeWindow xPos/yPos: %WinCoordX%/%WinCoordY%, Width/Height: %WinSizeW%/%WinSizeH%

	ifWinNotExist, %WinTitle%
	{
		run, clock.bat %Screen% "%WinTitle%"
		;MsgBox %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%	
		;WinWait , WinTitle, WinText, Seconds, ExcludeTitle, ExcludeText
		WinWait, %WinTitle%
	}
	WinMove, %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%
	GroupAdd, %GroupName%, %WinTitle%
}

Waveforms_NW(x,y,width,height)		; Waveforms - Top Left Screen
{	
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)"
	Display_OpenWindow(x,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)
	
	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)"
	Display_OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)
}			

Waveforms_NE(x,y,width,height)		; Waveforms - Top Right Screen
{	
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)"
	Display_OpenWindow(x,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)
	
	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)"
	Display_OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)

}

Waveforms_SW(x,y,width,height)		; Waveforms - Bottom Left Screen
{

	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 0

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)"

	Display_OpenWindow(x,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)"
	Display_OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)
}

Waveforms_SE(x,y,width,height)		; Waveforms - Bottom Right Screen
{
	If DebugLev2
		MsgBox, [In-Monitor 1-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%

	;Add Vertical Offset to y to allow for height of clock pannel
	VOffset := 120

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)"
	Display_OpenWindow(x,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)

	;** Window is already open **
	Title := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)"
	Display_OpenWindow(x+width/2,y+VOffset,width/2,height-Voffset, Title, LayoutGroupName)
}

Clock_Panel(x,y,width,height)
{
	If DebugLev2
		MsgBox, [In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
	Link := ShortCutDir . "Clock_Panel.lnk"
	Title := "Clock Panel"
	Display_LNKWindow(x, y, width, height, Link, Title, LayoutGroupName)
}

Antelope_Earthquakes(x,y,width,height)		; Antelope World Earthquakes
{
	Link  := ShortCutDir . "Antelope\EQ_Events.lnk"
	Title := "Integrated Geophysical Networks, Seismic Data Home Page"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)
	WinGet, WindowID, ID, A

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 1-4] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
		)

	CoordMode, Mouse, Client

	;EATWS x16 Client Coords for Buttons
	RowY	:= 70
	ViewX	:= 214
	BkgX	:= 400
	MapX	:= 640
	RowDY	:= 37
	MSpeed	:= 10
	
	; Change Map = Earthquake (Opt2)
	If DebugLev2
		MsgBox, SelectButton(%MapX%, %RowY%, %RowDY%, 2, %MSpeed%)
	Display_SelectButton(MapX, RowY, RowDY, 2, MSpeed)

	; Change View = World (Opt5)
	If DebugLev2
		MsgBox, SelectButton(%ViewX%, %RowY%, %RowDY%, 5, %MSpeed%)
	Display_SelectButton(ViewX, RowY, RowDY, 5, MSpeed)	
}

Antelope_Stations(x,y,width,height)	;World Station Latency
{
	Link  := ShortCutDir . "Antelope\Station_Latency.lnk"
	Title := "rhe-eqm-ops-prod.prod.lan/widescreen/latency.html"

	Display_LNKWindow(x,y,width,height, Link, Title, LayoutGroupName)

	WinGet, WindowID, ID, A

	If DebugLev2
		MsgBox, 
		(
			[In-Monitor 2-1] xPos/yPos: %x%/%y%, xStep/yStep: %width%/%height%
			Link = %Link%
			Active Window: %Title% - ID: %WindowID%
		)

	CoordMode, Mouse, Client

	;EATWS x16 Client Coords - Different to EQ_Events!!
	RowY	:= 30
	ViewX	:= 210
	BkgX	:= 390
	MapX	:= 635
	RowDY	:= 36
	MSpeed	:= 10
	
	; Change View = World (Opt5)
	If DebugLev2
		MsgBox, SelectButton(%ViewX%, %RowY%, %RowDY%, 3, %MSpeed%)
	Display_SelectButton(ViewX, RowY, RowDY, 5, MSpeed)
}

Load_WinCoords(Mode)
{
;	Screen/Window Coordinates:
;	===========================================================================
;	WinCoords := []		initiate the array object in Head before calling function
;	WinCoords [ScreenNo].x & [ScreenNo].y = Top Left Coordinate of Window
;	WinCoords[0].width 	= Width 
;	WinCoords[0].height	= Height
;	
;	EATWS Video Wall = 8 x 2 = 16 Monitors
;		Width = 8 x 1920 = 15360
;		Height = 2 x 1080 = 2160
;
;	2 Screen Test Mode = 2 Monitors
;		Width = 2 x 1920 = 3840
;		Height = 1080

	If DebugLev2
		MsgBox % "In Load_WinCoords with Mode = " . Mode

	if (Mode = "Local")
	{
		StepX := 1920
		StepY := 1080
		BaseY := 0
		TopY  := -1080
	}
	else if (Mode = "Test")
	{	; for dual monitors a suitable test mode is 640 x 540
		StepX := 680
		StepY := 540
		BaseY := 540
		TopY  := 0
	}
	
	If DebugLev2
		MsgBox % "Load_WinCoords: StepX,StepY,BaseY,TopY: " . StepX . "," . StepY . "," . BaseY . "," . TopY
			
	;Screens 	Width and Height
	;-------------------------------------------------
	WinCoords[0] := {width: StepX, height: StepY}

	;Bottom VideoWall Screens 	1 to 5 (Left to Right)
	;-------------------------------------------------
	WinCoords[1] 	:= {x: StepX * 0, y: BaseY}	; Bottom Left: Y = 0 or 540
	WinCoords[2] 	:= {x: StepX * 1, y: BaseY}
	WinCoords[3] 	:= {x: StepX * 2, y: BaseY}
	WinCoords[4] 	:= {x: StepX * 3, y: BaseY}
	WinCoords[5] 	:= {x: StepX * 4, y: BaseY}	; Bottom Right - Placements X+480, Y=270

	; Top VideoWall Screens 		6 to 10 (Left to Right)
	; --------------------------------------------------
	; Top of Bottom Screen = 0 / Top of Top Screen = -1080
	WinCoords[6] 	:= {x: StepX * 0, y: TopY}	; Top Left: Y = -1080 or 0
	WinCoords[7] 	:= {x: StepX * 1, y: TopY}
	WinCoords[8] 	:= {x: StepX * 2, y: TopY}
	WinCoords[9] 	:= {x: StepX * 3, y: TopY}
	WinCoords[10] 	:= {x: StepX * 4, y: TopY}	; Top Right

	;Non-Visible Screens 11 to 16
	;--------------------------------------------
	; Third In from Far Right	11=Top/12=Bottom
	WinCoords[11] 	:= {x: StepX * 5, y: TopY}
	WinCoords[12] 	:= {x: StepX * 5, y: BaseY}
	;--------------------------------------------
	; Second In from Far Right	13=Top/14=Bottom
	WinCoords[13] 	:= {x: StepX * 6, y: TopY}
	WinCoords[14] 	:= {x: StepX * 6, y: BaseY}
	;--------------------------------------------
	;Far Right					15=Top/16=Bottom
	WinCoords[15] 	:= {x: StepX * 7, y: TopY}
	WinCoords[16] 	:= {x: StepX * 7, y: BaseY}
	;--------------------------------------------
	
	Return 	; WinCoords[]
}


Load_AntelopeTitles()
{
;	Antelope Window Titles:
;	===========================================================================
;	AntelopeTitle := [] 	Initiate the array object in Head before calling function

	If DebugLev2
		MsgBox, Loading Antelope Window Titles

	AntelopeTitle[1] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Europe Africa Stations)"
	AntelopeTitle[2] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Indonesian  Stations)"
	AntelopeTitle[3] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Western/Central Australia)"
	AntelopeTitle[4] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Eastern Australia)"
	AntelopeTitle[5] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (W Pacific Stations)"
	AntelopeTitle[6] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (E Pacific Stations)"
	AntelopeTitle[7] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 1)"
	AntelopeTitle[8] := "GEOSCIENCE AUSTRALIA REAL-TIME SEISMIC MONITOR (Regional Stations 2)"
	Return AntelopeTitle[]
}

Load_ScreenTest(GroupName)
{
	If DebugLev2
		MsgBox, Loading Windows with Mode = SingleScreenTest

	AlertLink := ShortCutDir . "Antelope\Alert1.lnk"
	
	Loop, % AntelopeTitle.MaxIndex()
	{
		WinTitle = % AntelopeTitle[A_Index]
		WinCoordX = % WinCoords[A_Index].x		; Not Final Position - but highlights that window exists
		WinCoordY = % WinCoords[A_Index].y		; Not Final Position - but highlights that window exists
		WinSizeW = % WinCoords[0].width
		WinSizeH = % WinCoords[0].height
		If DebugLev2
			MsgBox % A_Index . " = " . WinTitle . ": " . WinCoordX . "/" . WinCoordY . "/" . WinSizeW . "/" . WinSizeH
			
		If !WinExist(WinTitle)
		{
			; TODO: Presume that non of the Antelope windows exist - fix this presumption later
			; run, clock.bat %A_Index% "%WinTitle%"
			run, %AlertLink%
			sleep 20000			; wait 10 seconds...
			
			If DebugLev2
				MsgBox %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%	
			WinWait, %WinTitle%
			WinMove, %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%
		}
		Else	; Window Exists
		{
			If DebugLev2
				MsgBox, Window "%WinTitle%" Exists
			WinMove, %WinTitle%, , %WinCoordX%, %WinCoordY%, %WinSizeW%, %WinSizeH%
		}
		If WinExist(WinTitle)
			GroupAdd, %GroupName%, %WinTitle%
	}
}

Call_AntelopeGroupHotKeys()
{
	;	Get a list of Window IDs from ahk_group and pass to HotKey Script as single string:
	;	===================================================================================

	WinGet,GroupList,list,ahk_group %LayoutGroupName% 
	GroupListText := ""
	Loop % GroupList
	{
		;MsgBox % GroupList%A_Index%

		;	GroupListText := GroupListText . GroupList%A_Index%
		temp := GroupList%A_Index%
		if (A_Index=1)
			GroupListText = %temp%
		else	
			GroupListText = %GroupListText%,%temp%
		
		;MsgBox, % GroupListText
	}
	If DebugLev2
		MsgBox, GroupListText: %GroupListText%
		
	Run AntelopeGroup_HotKeys.exe %GroupListText%
	return
}

Stop_ExistingAntelopeGroup_HotKeys()
{
	DetectHiddenWindows, On
	SetTitleMatchMode, 2
	ProcessName = "AntelopeGroup_HotKeys.exe"
	If WinExist(ProcessName)
	{
		If DebugLev2
			MsgBox, ProcessName: %ProcessName% Running - so Close
		Process, Close , %ProcessName%
	} Else {
		If DebugLev2
			MsgBox, Process not found
	}
	DetectHiddenWindows, Off
	Return
}