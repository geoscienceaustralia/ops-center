; 	Banner.ahk
;	
;	Utility to display a banner over existing windows
;
;	Tony Pack - October 2019
;
;	Options:

	HelpStr := "Display a transparent banner over existing windows"
	. "`n  -h or --help	Display this help screen (ignores other options)"
	. "`n  -c or --close Close all existing open banners (ignores other options)"
	. "`n  /L	GUI Name and Window Title. Default = 'Banner'"
	. "`n  /K	Background color. Any RGB Hex Colour."
	. "`n   	Default = 000000 = Black"
	. "`n  /P	Background transparency."
	. "`n   	Range of 0 = Transparent, to 255 = Opaque."
	. "`n   	Default = 150"
	. "`n  /B	Subsequent text lines... Not yet implemented"
	. "`n  /S	FontSize in Points. Default = 32"
	. "`n  /C	FontColor: MS Windows 16 Color palette names "
	. "`n   	or RGB Hex Colors. Default = Black"
	. "`n    	i.e. black, maroon, green, olive, navy, purple,"
	. "`n   	teal, silver, gray, red, lime, yellow, blur, fuchsia, aqua, white"
	. "`n  /W	Width of banner in pixels. Default = fit text"
	. "`n  /H	Height of banner in pixels. Default = fit text"
	. "`n  /X	Horizontal Position. Default = centre on screen"
	. "`n  /Y	Vertical Position. Default = centre on screen"
	. "`n  /M	Left/Right Margin."
	. "`n   	Default = 1.25 times font-height for left & right = 38"
	. "`n  /N	Top/Bottom Margin."
	. "`n   	Default = = 0.75 times font-height for top & bottom = 24"
	
;	---------------------------------------------------------------------------

	#SingleInstance off			; Allow multiple instances of this script to run concurrently
	Debug := False
	
;	Define Default Values - for use if no paramaters are shown
;	===========================================================================

	Title := "Banner"			; "/L="	
	BackColor := "000000"		; "/K="	000000 = Black - Can be any RGB Hex Colour
	Transparency := 150			; "/P="	Range of 0 = fully transparent, to 255 = not transparent

	Text := "banner.exe -h for Help"	; "/T="	Text to Display - Line 1
	Body := "Body..."			; "/B="	Subsequent text lines...
	FontSize := 32				; "/S="
	FontColor := "White"		; "/C="
	Width := 300				; "/W="
	Height := 100				; "/H="
	PosX := 100					; "/X="
	PosY := 100					; "/Y="
	XMargin := 38				; "/M=" Default = 1.25 times font-height for left & right
	YMargin := 24				; "/N=" Default = 0.75 times font-height for top & bottom
	
	GUI_ShowOptions := "NoActivate"			; NoActivate avoids deactivating the currently active window.
	GUI_TextOptions := "center"		; Options for Text - eg. Align Centre, width, colour.
	
;	Check if any paramaters have been received
;	===========================================================================

	for n, param in A_Args  ; For each parameter:
	{
		If InStr(param, "-c", ,1) or InStr(param, "--close", ,1)				; Close all Banners and Exit
		{
			instNum := 0
			Loop 
			{
				Process, Exist, banner.exe
				If ErrorLevel
				{	
					++instNum
					Process, Close, %Errorlevel%
				}
			}
			ExitApp
		}
		Else If InStr(param, "--help", ,1) or InStr(param, "-h")	; Display help info
		{
			MsgBox % HelpStr
			ExitApp
		}
		Else If InStr(param, "/L=", ,1)				; Title for Window - AHK Reference
		{
			Title := SubStr(param, 4)
		}
		Else If InStr(param, "/T=", ,1)			; Banner Text (Main)
		{
			Text := SubStr(param, 4)
		}
		Else If InStr(param, "/W=", ,1)			; Width - GUI_TextOptions
		{
			Width := SubStr(param, 4)
			GUI_TextOptions .= " W" . Width
			If Debug
				MsgBox % "GUI_TextOptions: " . GUI_TextOptions
		}
		Else If InStr(param, "/H=", ,1)			; Height
		{
			Height := SubStr(param, 4)
			GUI_ShowOptions .= " H" . Height
			If Debug
				MsgBox % "GUI_ShowOptions: " . GUI_ShowOptions
		}
		Else If InStr(param, "/X=", ,1)			; X Position
		{
			PosX := SubStr(param, 4)
			GUI_ShowOptions .= " X" . PosX
			If Debug
				MsgBox % "GUI_ShowOptions: " . GUI_ShowOptions
		}
		Else If InStr(param, "/Y=", ,1)			; Y Position
		{
			PosY := SubStr(param, 4)
			GUI_ShowOptions .= " Y" . PosY
			If Debug
				MsgBox % "GUI_ShowOptions: " . GUI_ShowOptions
		}
		Else If InStr(param, "/P=", ,1)			; Transparency
		{
			Transparency := SubStr(param, 4)
		}
		Else If InStr(param, "/K=", ,1)			; BackColor
		{
			BackColor := SubStr(param, 4)
		}
		Else If InStr(param, "/S=", ,1)			; FontSize
		{
			FontSize := SubStr(param, 4)
			XMargin := FontSize * 1.25
			YMargin := FontSize * 0.75 
		}
		Else If InStr(param, "/C=", ,1)			; FontColor - GUI_TextOptions
		{
			FontColor := SubStr(param, 4)
			GUI_TextOptions .= " c" . FontColor
			If Debug
				MsgBox % "GUI_TextOptions: " . GUI_TextOptions
		}
		Else If InStr(param, "/M=", ,1)			; X Margin
		{
			XMargin := SubStr(param, 4)
		}
		Else If InStr(param, "/N=", ,1)			; Y Margin
		{
			YMargin := SubStr(param, 4)
		}
	}
	
	If Debug
	{
		MsgStr := "Banner Options:"
		. "`n 	Title [/L=]: " . Title
		. "`n 	BackColor [/K=]: " . BackColor
		. "`n 	Transparency [/P=]: " . Transparency
		. "`n 	Text [/T=]: " . Text
		. "`n 	Body [/B=]: " . Body
		. "`n 	FontSize [/S=]: " . FontSize
		. "`n 	FontColor [/C=]: " . FontColor
		. "`n 	Width [/W=]: " . Width
		. "`n 	Height [/H=]: " . Height
		. "`n 	PosX [/X=]: " . PosX
		. "`n 	PosY [/Y=]: " . PosY
		. "`n 	XMargin [/M=]: " . XMargin
		. "`n 	YMargin [/N=]: " . YMargin
		MsgBox % MsgStr
	}

;	Close Banner if already exists
;	===========================================================================

	If WinExist(Title)		; Close existing window with same title - Ensures that only uniquely title banners exist
		WinClose

;	Display Banner
;	===========================================================================

;	RowHeight := FontSize + 10
	If Debug
		MsgBox % "GUI_TextOptions: " . GUI_TextOptions . "`n GUI_ShowOptions: " . GUI_ShowOptions
	
	
	Gui, New, +LastFound +AlwaysOnTop -Caption +ToolWindow, %Title%
	Gui, Color, %BackColor%
	Gui, Margin, %XMargin%, %YMargin%
	Gui, Font, s%FontSize% c%FontColor% 	; Set a large font size (32-point).

;	Gui, Add, Text, center w%Width% c%FontColor%, %Text% 
	Gui, Add, Text, %GUI_TextOptions%, %Text% 
	
;	WinSet, TransColor, %BackColor% %Transparency%
	WinSet, Transparent, %Transparency%				; transparency for text and background

	Gui, Show, x%PosX% y%PosY% h%Height% NoActivate  ; NoActivate avoids deactivating the currently active window.
;	Gui, Show, %GUI_ShowOptions%  					
	