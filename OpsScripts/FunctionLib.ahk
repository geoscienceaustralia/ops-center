;	FunctionLib.ahk
;
; 	Scripts Library File
;
; 	Tony Pack - September 2019
;
;	Functions:
;		LoadGlobals
;		CheckDisplays
;		LoadMonitorsArray
;		LoadWindowsArray
;		Eval Equations Functions
;		
;	===========================================================================


LoadGlobals(GlobalsFile)
{	
;	Globals need to be defined in calling script prior to beng loaded.
;	
;	The following section is the reference for Globals loaded here:

/*
;	Define Global Values for use by Config File - Copy for Reference
;	===========================================================================
	
	;[Config_Files]
	global MonitorsFile				; define dimensions and layout of monitors
	global WindowsFile				; define content of windows to display
	
	;[Initiatalisation]
	global WinWaitTime				; wait time for WinWait command
	global TitleMatchMode			; SetTitleMatchMode: 1 = Starts With, 2 = Contains, 3 = Exact Match
	
	;[Debug]
	global Debug					; turn on general Debugging
	global DebugLev2				; turn on detailed Debugging
	global DebugConfig				; turn on Debugging for Configuration
	global DebugDisplay				; turn on Debugging for 
	global DebugMonitorsInit		; turn on Debugging for 
	global DebugWindowInit			; turn on Debugging for 	
	global DebugWindowDisplay		; turn on Debugging for 
	
	;[BrowserCMD]
	global BrowserCMD				; Command used to open browser windows (eg Google Chrome). To be followed by the URL
	global BrowserCMDSuffix			; Any suffix command to be added after the window URL
	
	;[BannerCMD]
	global BannerCMD
	
*/
	
	IniRead, OutputVarSectionNames, %GlobalsFile%		; Load Section Names from Globals Ini File

	If Debug
		MsgBox % "Configuration File " . GlobalsFile . " loaded with following Sections: `n" . OutputVarSectionNames . " " . OutputVarSectionNames.MaxIndex()	
	
	loop, parse, OutputVarSectionNames, `n
	{
		If (A_LoopField = "Config_Files")	; Define Monitor and Windows Config Files
		{
			IniRead, MonitorsFile, %GlobalsFile%, %A_LoopField%, Monitors
			IniRead, WindowsFile, %GlobalsFile%, %A_LoopField%, Windows
		}
		Else If (A_LoopField = "Initiatalisation")	; Define Global Variables - 
		{
			IniRead, WinWaitTime, %GlobalsFile%, %A_LoopField%, WinWaitTime		; Wait 5 seconds before continuing
			IniRead, TitleMatchMode, %GlobalsFile%, %A_LoopField%, TitleMatchMode	; TitleMatchMode: 1 = Starts With, 2 = Contains, 3 = Exact Match
			if TitleMatchMode
				SetTitleMatchMode, %TitleMatchMode%
		}	
		Else If (A_LoopField = "Debug")	; Define Global Variables
		{
			IniRead, Debug, %GlobalsFile%, %A_LoopField%, DebugLev1			; Basic Debug information
			IniRead, DebugLev2, %GlobalsFile%, %A_LoopField%, DebugLev2		; More detailed Debug information
			IniRead, DebugDisplay, %GlobalsFile%, %A_LoopField%, DebugDisplay		; More detailed Info - Displays
			IniRead, DebugMonitorsInit, %GlobalsFile%, %A_LoopField%, DebugMonitorsInit		; More detailed Info - Monitors Init
			IniRead, DebugWindowInit, %GlobalsFile%, %A_LoopField%, DebugWindowInit		; More detailed Info - Windows Init
			IniRead, DebugWindowDisplay, %GlobalsFile%, %A_LoopField%, DebugWindowDisplay		; More detailed Info - Windows Load
		}			
		Else If (A_LoopField = "BrowserCMD")	; Define Global Variables
		{
			IniRead, BrowserCMD, %GlobalsFile%, %A_LoopField%, BrowserCMD
			IniRead, BrowserCMDSuffix, %GlobalsFile%, %A_LoopField%, BrowserCMDSuffix
		}	
		Else If (A_LoopField = "BannerCMD")		; Define Global Variables
		{
			IniRead, BannerCMD, %GlobalsFile%, %A_LoopField%, BannerCMD
		}			
	}

	If Debug
	{
		MsgStr := "Global Values Loaded from: " . GlobalsFile
			. "`n  MonitorsFile: " . MonitorsFile
			. "`n  WindowsFile: " . WindowsFile
			. "`n  WinWaitTime: " . WinWaitTime
			. "`n  TitleMatchMode: " . TitleMatchMode
			. "`n  DebugLev1: " . Debug
			. "`n  DebugLev2: " . DebugLev2
			. "`n  BrowserCMD: " . BrowserCMD
			. "`n  BrowserCMDSuffix: " . BrowserCMDSuffix
			. "`n  BannerCMD: " . BannerCMD
			
		MsgBox % MsgStr		
	} 	
}
;	---------- End Function: LoadGlobals --------------------------------------


;	Get Video Wall dimensions - Set Test/Live Mode
;	===========================================================================
;	1 Screen Test Mode (1 Monitor/Laptop) = Width = 1920 by Height = 1080
;
;	2 Screen Test Mode (2 Monitors) = Width = (2 x 1920) = 3840 by Height = 1080

CheckDisplays()
{
	SysGet, VirtualScreenWidth, 78
	SysGet, VirtualScreenHeight, 79
	MaxScreenWidth := VirtualScreenWidth
	MaxScreenHeight := VirtualScreenHeight

	If (MaxScreenWidth = 1920)
	{
		Mode := "Test1"
		MonitorsFile := ".\Monitors_TestMode1.ini"
		MsgBox % "Running in Single Screen Test Mode `nScreen Dimensions: " . MaxScreenWidth . " x " . MaxScreenHeight
	}
	Else If (MaxScreenWidth = 3840)
	{
		Mode := "Test2"
		MonitorsFile := ".\Monitors_TestMode2.ini"
		MsgBox % "Running in Dual Screen Test Mode `nScreen Dimensions: " . MaxScreenWidth . " x " . MaxScreenHeight
	}
	Else If (MaxScreenWidth = 6720)
	{
		Mode := "Test3"
		MonitorsFile := ".\Monitors_TestMode3.ini"
		MsgBox % "Running in Dual Screen (Surface) Test Mode `nScreen Dimensions: " . MaxScreenWidth . " x " . MaxScreenHeight
	}

	If Debug
	{
		MsgBox % "DebugLev2: " . DebugLev2
		MsgBox % "Monitor Dimensions: " . MaxScreenWidth . " x " . MaxScreenHeight
		MsgBox % "Mode: " . Mode
		MsgBox % "ShortCutDir: " . ShortCutDir
	}
}
;	---------- END Get Video Wall dimensions ----------------------------------


;	Load Monitor Array:
;	===========================================================================
; 	Load Monitor Definitions and save to WinCoords array
;	Uses global MonitorsFile and loads values into global WinCoords[] array

LoadMonitorsArray()
{
;	Uses global WinCoords := []		; Size and Coordinates for Windows
;	For consistency: Monitors should be identified by the number that identifies them using the Windows Display 
;	Settings tool which is opened by right-clicking on the desktop and selcting Display Settings menu option.
; 	Click the Identify button to show Monitor numbers
;
;	The Monitor ID Number (starting at 1 to n...) is used in the Windows Config file to map windows to monitors 

	If !FileExist(MonitorsFile)
	{
		MsgBox % "Monitors Config File: " . MonitorsFile . " not found`nScript will exit"
		ExitApp
	} 
	
	IniRead, OutputVarSectionNames, %MonitorsFile%		; Load Section Names from Ini File
	
	If (Debug OR DebugMonitorsInit)
		MsgBox % "Monitors Config File " . MonitorsFile . " loaded with following Sections: `n" . OutputVarSectionNames . " " . OutputVarSectionNames.MaxIndex()
	
	loop, parse, OutputVarSectionNames, `n		; Loop through Config file sections
	{
		If InStr(A_LoopField, "Monitor-", 1)	; If a section title starts with "Monitor-" then process
		{
			Monitor := SubStr(A_LoopField, 9)	; Use the Section Title Suffix as the monitor ID - index in WinCords array
			if WinCoords[Monitor]	; Check if the Monitor has already been defined - Exit if already exists - Config file error.
			{
				MsgBox % "The Monitor " . Monitor . " has already been defined - check the Monitor Config file: " . MonitorsFile 
					. "`nScript will Terminate"
					
				ExitApp
			}

			; ToDo - add error handling for missing x,y,width and height values. Key & key2 are optional.
			IniRead, key, %MonitorsFile%, %A_LoopField%, key, %A_Space%			; HotKey to move cursor to the Monitor
			IniRead, key2, %MonitorsFile%, %A_LoopField%, key2, %A_Space%		; Alternate HotKey - as above

			IniRead, x, %MonitorsFile%, %A_LoopField%, x, %A_Space%
			IniRead, y, %MonitorsFile%, %A_LoopField%, y, %A_Space%
			IniRead, width, %MonitorsFile%, %A_LoopField%, width, %A_Space%
			IniRead, height, %MonitorsFile%, %A_LoopField%, height, %A_Space%
			
			If DebugMonitorsInit 
				MsgBox, 
				(LTrim	
					Monitor Details:
					Monitor: %Monitor%
					key: %key%
					key2: %key2%
					x: %x%
					y: %y%
					width: %width%
					height: %height%
				)
			
			WinCoords[Monitor] := {key: key
				, key2: key2
				, x: x
				, y: y
				, width: width
				, height: height}
		}
	}
}
;	---------- END Load Monitor Array -----------------------------------------


;	Load Window Array:
;	===========================================================================
; 	Load Window Definitions and save to WinDef array
; 	Uses global WindowsFile and load values into global WinDef[] array

LoadWindowsArray()
{
	If !FileExist(WindowsFile)
	{
		MsgBox % "Windows Config File: " . WindowsFile . " not found`nScript will exit"
		ExitApp
	} 

	IniRead, OutputVarSectionNames, %WindowsFile%		; Load Section Names from Ini File
	
	If (Debug OR DebugWindowInit)
	{
		MsgBox, 4, , % "Init File loaded with following Sections: `n" . OutputVarSectionNames . " " . OutputVarSectionNames.MaxIndex()
		IfMsgBox, No, ExitApp
	}
	loop, parse, OutputVarSectionNames, `n		; For each window definition in window.ini file - loop through sections
	{
		If InStr(A_LoopField, "WINDOW", 0)		; Window Section - not case sensitive
		{	
			Window := A_LoopField
			IniRead, title, %WindowsFile%, %A_LoopField%, title, %A_Space%
			IniRead, alwaysOnTop, %WindowsFile%, %A_LoopField%, alwaysOnTop, %A_Space%
			IniRead, monitor, %WindowsFile%, %A_LoopField%, monitor, %A_Space%
			
			IniRead, case, %WindowsFile%, %A_LoopField%, case, %A_Space%
			IniRead, src, %WindowsFile%, %A_LoopField%, src, %A_Space%
			IniRead, sendcmd, %WindowsFile%, %A_LoopField%, sendcmd, %A_Space%
			
			IniRead, OffsetX, %WindowsFile%, %A_LoopField%, xOff, %A_Space%
			IniRead, OffsetY, %WindowsFile%, %A_LoopField%, yOff, %A_Space%
			
			IniRead, width, %WindowsFile%, %A_LoopField%, width, %A_Space%
			IniRead, height, %WindowsFile%, %A_LoopField%, height, %A_Space%

			IniRead, LoginTitle, %WindowsFile%, %A_LoopField%, LoginTitle, %A_Space%
			IniRead, LoginSendcmd, %WindowsFile%, %A_LoopField%, LoginSendcmd, %A_Space%

			IniRead, WindowSleep, %WindowsFile%, %A_LoopField%, WindowSleep, %A_Space%
			IniRead, WindowMsg, %WindowsFile%, %A_LoopField%, WindowMsg, %A_Space%

			WinDef[Window] := {Title: title
				,AlwaysOnTop: alwaysOnTop
				,Case: case
				,SRC: src
				,SendCMD: sendcmd
				,Monitor: monitor
				,OffsetX: OffsetX
				,OffsetY: OffsetY
				,width: width
				,height: height
				,LoginTitle: LoginTitle
				,LoginSendcmd: LoginSendcmd 
				,WindowSleep: WindowSleep
				,WindowMsg: WindowMsg}

			If DebugWindowInit 
			{
				MsgStr := "`n  Section: " . A_LoopField
					. "`n  Title: " . WinDef[Window].Title
					. "`n  AlwaysOnTop: " . WinDef[Window].alwaysOnTop
					. "`n  Case: " . WinDef[Window].case
					. "`n  SRC: " . WinDef[Window].src
					. "`n  SendCMD: " . WinDef[Window].sendcmd
					. "`n  Monitor: " . WinDef[Window].monitor
					. "`n  OffsetX: " . WinDef[Window].OffsetX
					. "`n  OffsetY: " . WinDef[Window].OffsetY
					. "`n  width: " . WinDef[Window].width
					. "`n  height: " . WinDef[Window].height
					. "`n  LoginTitle: " . WinDef[Window].LoginTitle
					. "`n  LoginSendcmd: " . WinDef[Window].LoginSendcmd
					. "`n  WindowSleep: " . WinDef[Window].WindowSleep
					. "`n  WindowMsg: " . WinDef[Window].WindowMsg
			
				MsgBox % "Window Details [Write Array]: " . MsgStr
			}
		}
	}	
}
;	---------- END Load Window Array ------------------------------------------




; Eval Equations Functions
; =============================================================================
; evaluate arithmetic expressions containing:
; unary +,- (-2*3; +3)
; +,-,*,/,\(or % = mod); **(or @ = power)
; (..); var (pi, e); abs(),sqrt(),floor()
; From: https://autohotkey.com/board/topic/4779-simple-script-for-evaluating-arithmetic-expressions/://autohotkey.com/board/topic/4779-simple-script-for-evaluating-arithmetic-expressions/page-2?&#entry101504
; Simple script for evaluating arithmetic expressions
; by User: Laszlo, Aug 23 2005 10:55 PM
;
; Use Examples:
/*
	height := 1000
	equation := "(height*2)/4"
	NewEq := StrReplace(equation, "height" , %height%)		; Returns "(1000*2)/4"

	MsgBox % Eval("floor(" . equation . ")")	; Returns 500

	;MsgBox % Eval("-floor(abs(sqrt(1))) * (+pi -((3%5))) +pi+ 2-1-1 + e-abs(sqrt(floor(2)))**2-e") ; 1
*/

Eval(x) {                              ; expression preprocessing
	Static pi = 3.141592653589793, e = 2.718281828459045

	StringReplace x, x,`%, \, All       ; % -> \ for MOD
	x := RegExReplace(x,"\s*")          ; remove whitespace
	x := RegExReplace(x,"([a-zA-Z]\w*)([^\w\(]|$)","%$1%$2") ; var -> %var%
	Transform x, Deref, %x%             ; dereference all %var%

	StringReplace x, x, -, #, All       ; # = subtraction
	StringReplace x, x, (#, (0#, All    ; (-x -> (0-x
	If (Asc(x) = Asc("#"))
		x = 0%x%                         ; leading -x -> 0-x
	StringReplace x, x, (+, (, All      ; (+x -> (x
	If (Asc(x) = Asc("+"))
		StringTrimLeft x, x, 1           ; leading +x -> x
	StringReplace x, x, **, @, All      ; ** -> @ for easier process

	Loop {                              ; find innermost (..)
		If !RegExMatch(x, "(.*)\(([^\(\)]*)\)(.*)", y)
			Break
		x := y1 . Eval@(y2) . y3         ; replace "(x)" with value of x
	}
	Return Eval@(x)                     ; no more (..)
}

Eval@(x) {
	RegExMatch(x, "(.*)(\+|\#)(.*)", y) ; execute rightmost +- operator
	IfEqual y2,+,  Return Eval@(y1) + Eval@(y3)
	IfEqual y2,#,  Return Eval@(y1) - Eval@(y3)
                                       ; execute rightmost */% operator
	RegExMatch(x, "(.*)(\*|\/|\\)(.*)", y)
	IfEqual y2,*,  Return Eval@(y1) * Eval@(y3)
	IfEqual y2,/,  Return Eval@(y1) / Eval@(y3)
	IfEqual y2,\,  Return Mod(Eval@(y1),Eval@(y3))
                                       ; execute rightmost power
	StringGetPos i, x, @, R
	IfGreaterOrEqual i,0, Return Eval@(SubStr(x,1,i)) ** Eval@(SubStr(x,2+i))
                                       ; execute rightmost function
	If !RegExMatch(x,".*(abs|floor|sqrt)(.*)", y)
		Return x                         ; no more function
	IfEqual y1,abs,  Return abs(  Eval@(y2))
	IfEqual y1,floor,Return floor(Eval@(y2))
	IfEqual y1,sqrt, Return sqrt( Eval@(y2))
}
;	---------- End Function: Eval Equations -----------------------------------

