;	MouseKeys.ahk - Mouse Hotkeys
;	
;	Tony Pack - January/July 2018
;
;	Utility for multiscreen display. Moves mouse to top left quadrant of selected screen using HotKeys Alt+1 to Alt+N...
;	
;	Number Lock has been disabled to ensure alway on
;	Duplicate Alt+Number keys enable to allow for non keypad keyboard - eg. AV Room
;
;	This utility loads the Local-Monitors.ini file and defines hotkeys to move cursor to selecgted monitor.
;
;	HIGHLIGHT Mouse Pointer
;	=======================
;	To turn on Control Key Mouse Highlight: Open Settings and search for "mouse"
;	Select "Mouse Settings" then select "Additional Mouse Options" from right hand side of screen
;	In pop-up window, select "Pointer Options" tab and tick the "Show location of pointer when I press the CRTL key"
;
;	TODO:
;		Move globals read to library file - as used by a number of apps...
;
;	---------------------------------------------------------------------------

	#SingleInstance force
	#Include FunctionLib.ahk		; Include Shared Function Library

	SetTitleMatchMode, 2			; Title Match Mode = can contain text anywhere in title
	global WinCoords := []		; Size and Coordinates for Windows
	global WinDef := []				; Definitions for displayed Windows


;	Define Global Values for use by Config File
;	===========================================================================
	global MonitorsFile				; define dimensions and layout of monitors
	global WindowsFile				; define content of windows to display (Not Used in this script)
	global WinWaitTime				; wait time for WinWait command - Not Used
	global Mode						; ?? not used
	global Debug := 0				; turn on general Debugging
	global DebugLev2				; turn on detailed Debugging
	global DebugConfig				; turn on Debugging for Configuration
	global DebugDisplay				; turn on Debugging for 
	global DebugMonitorsInit		; turn on Debugging for 
	global DebugWindowInit			; turn on Debugging for 	
	global DebugWindowDisplay		; turn on Debugging for 
	
	global BrowserCMD				; Command used to open browser windows (eg Google Chrome). To be followed by the URL - Not Used
	global BrowserCMDSuffix			; Any suffix command to be added after the window URL - Not Used
	
	
;	Load Global Values from Config File
;	===========================================================================
	GlobalsFile := ".\Globals.ini"
	
	If !FileExist(GlobalsFile)
	{
		MsgStr := "No Initialisation File. Please Create the file Globals.ini"
			. "`n  See Readme.txt for Information"
			. "`n  LoadWindows.exe will exit"
		MsgBox % MsgStr		
		ExitApp
	} 									

	LoadGlobals(GlobalsFile)

;	---------- END Load Globals -----------------------------------------------


	CheckDisplays()			; Checks Monitors actually connected to system and overrides config if running 1 or 2 screens.
	
	
	LoadMonitorsArray()		; Loads WinCoords array with definitions of available Monitors

	
;	Define HotKeys:
;	===========================================================================
;	Load each HotKey from WinCoords array and open
	
	Loop % WinCoords.Length()	
	{
;		MsgBox % "Key: " . WinCoords[A_Index].key . " - " . StrLen(WinCoords[A_Index].key);
;		. "`nKey2: " . WinCoords[A_Index].key2 . " - " . StrLen(WinCoords[A_Index].key2)
		
		if !(WinCoords[A_Index].key = "")
		{
			key := WinCoords[A_Index].key		
			fn := Func("MoveCursor").Bind(A_Index)
			hotkey, $%key%, % fn
			If DebugMonitorsInit
				MsgBox % "Defined HotKey: " . key . " for Monitor " . A_Index . "`n" . WinCoords[A_Index].x	. "/" . WinCoords[A_Index].y
		}
		if !(WinCoords[A_Index].key2 = "")
		{
			key := WinCoords[A_Index].key2		
			fn := Func("MoveCursor").Bind(A_Index)
			hotkey, $%key%, % fn
			If DebugMonitorsInit
				MsgBox % "Defined HotKey: " . key . " for Monitor " . A_Index . "`n" . WinCoords[A_Index].x	. "/" . WinCoords[A_Index].y
		}		
	}

		
/*	
	; 	Add section to load additional Hotkeys from Monitors.ini config file
	
		Else If InStr(A_LoopField, "HotKey-", 1)	; Additional Hotkeys
		{
			Monitor := SubStr(A_LoopField, 9)
			IniRead, key, %MonitorsFile%, %A_LoopField%, key
			IniRead, command, %MonitorsFile%, %A_LoopField%, command
			If Debug 
				MsgBox, 
				(LTrim	
					HotKey Details:
					key: %key%
					command: %command%
				)
					
			; Define Hotkeys Function
			fn := Func("MoveCursor").Bind(Monitor)
			hotkey, $%key%, % fn
		}
*/
;	---------- END Define HotKeys ---------------------------------------------


/*
;	Open GNSS Windows:
;	---------------------------------------------------------------------------

!G::
	Run, LoadWindows-init.exe GNSS.ini
	return
*/	


;	Functions:
;	===========================================================================
	
MoveCursor(Monitor, OffsetX := 100, OffsetY := 100)
{
	DllCall("SetCursorPos", int, WinCoords[Monitor].x + OffsetX, int, WinCoords[Monitor].y + OffsetY)
	SendCtrl()
	return
}

sendCtrl(wait:=250) {
	Send {Ctrl down}{Ctrl up}
	Sleep %wait%
	Send {Ctrl down}{Ctrl up}
}
