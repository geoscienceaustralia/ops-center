; --------------------------------------------------------- 
; AntiSleep.ahk
;
;	AntiSleep checks the number of milliseconds that have ellapsed since the system last received
;	a user input (keyboard/mouse/etc) - after every 10 minutes of inaction, the mouse is moved to
;	reset system activity.
;
;	Tony Pack - September 2019
;
; --------------------------------------------------------- 

CoordMode, Mouse, Screen

Loop {
	if (A_TimeIdle > 10000) {
		; don't do anything unless no user input for at least 10 seconds
		MouseGetPos, xpos, ypos 
		MouseMove, xpos, ypos
		MouseMove, xpos+20, ypos+20, 10
		MouseMove, xpos-20, ypos-20, 10
		MouseMove, xpos, ypos
		Sleep, 600000	; Sleep for 10 minutes: 1000 msec = 1 second
	}
}