# Operation Centre Scripts
This directory contains a number of compiled scripts for automating the operation of multi-monitor video wall setups - such as for Operation Centres. 

The scripts were originally writen to automatically load windows for the Australian National Earthquake Alerting Centre's [NEAC] Operations Centre - and has since been expanded for other Operations Centres at Geoscience Australia. The app for the most part - by passes the need for an operator to physically place windows on multi-monitor setups, automates most reboot processes, and provides tools to find and move the cursor across a multi-monitor setup.

Developed using AutoHotKey [https://www.autohotkey.com/]

To enable to computer to auto-start required screens - you will need to copy a number of shortcuts to the startup directory. These files should be in the "startup" directory in this repo:  

Startup Directory: C:\Users\Default\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

1. antisleep.exe - to override screen locking mechanisms.
2. loadwindows.exe = to load required windows
3. 


## Load Windows Application

The LoadWindows.exe is the primary application for defining and loading browser or application windows on a multi-monitor setup.

Command Line Paramaters:

```
ToDo: Change logic to not run from source directory. Only option from source directory is a test config...
require "/O" (OpsCentre Directory) to point to Specific Configurations - default is Globals.ini in /O directory - and other paramaters e.g. /W to run alternative window configurations - e.g. for testing/new versions of windows etc.
```

- "-h" or "--help" - to load help screen
- /G - Load Globals File (e.g. /G.\Globals.ini) - Default: .\Globals.ini"
- /W - Load Windows Definition File - Default: defined in Globals.ini file"
- /D - Set Debug Status - Default: defined in Globals.ini file"
- /M - Load Monitor Definition File - Default: defined in Globals.ini file"
- /R - Reposition windows as defined in Windows Definition File without closing/restarting."
	

---

### Globals:

The Globals.ini file is used to define a number of environment variables - including links to Monitor and Windows .ini files. 

Used by:
- LoadWindows.exe
- MouseKeys.exe

See Also:
- Default Config File = Monitors.ini
- Default Config File = Windows.ini

Globals.ini Sections:
- [Config_Files] - Path to Monitors and Windows .ini files
- [Initiatalisation] - paramaters for AutoHotKey actions [Bold ones have been implemented - check others...]
    - **WinWaitTime** - define maximum wait time for WinWait command. If window does not appear within this time - code will skip to next window.
    - CMDWaitTime - 
    - Mode=Local
    - BannerClear=True
    - **TitleMatchMode**=1 - Mode to match window title names - Default = 1 = Starts With, 2 = Contains, 3 = Exact Match
- [Debug] - Debug settings - ToDo: Debug currently built into code - but may be replaced by coding environment.
- [BrowserCMD] - path to the default browser app with paramaters to minimise menus etc.
- [BannerCMD] - path to AutoHotKey script that can build banners - not yet well implemented.

```
; ========== Example Globals.ini File =====================================
[Config_Files]
Monitors="..\NEAC-Proc1\Monitors-NEAC_11xMonitors-Proc1.ini"
Windows="..\NEAC-Proc1\Windows-NEAC_11xMonitors-Proc1.ini"

[Initiatalisation]
WinWaitTime=25
CMDWaitTime=40
Mode=Local
BannerClear=True
; TitleMatchMode: 1 = Starts With, 2 = Contains, 3 = Exact Match
TitleMatchMode=1

[Debug]
DebugLev1=0
DebugLev2=0
DebugDisplay=0
DebugMonitorsInit=0
DebugWindowInit=0
DebugWindowDisplay=0

[BrowserCMD]
;BrowserCMD="C:\Program Files (x86)\Google\Chrome\Application\chrome_proxy.exe ---new-window --app="
BrowserCMD="C:\Program Files\Google\Chrome\Application\chrome.exe ---new-window --app="
BrowserCMDSuffix=""

[BannerCMD]
BannerCMD="C:\w10dev\OpsCentres\OpsScripts\banner.exe "
```
---

### Monitors:

The monitors.ini file contains the definition of the system monitors - including their size, coordinates, and shortcut key(s). The monitors.ini file can be named as desired and is loaded from the reference in the globals.ini file.

Each Monitor section must have a unique ID/Name. For ease of use/consistency, monitors may be identified by the number that identifies them using the Windows Display Settings app which is opened by right-clicking on the desktop and selcting Display Settings menu option. Click the Identify button to show Monitor numbers. Monitor width and height is the Display resolution as shown in Display Settings. The "WindowsSpy.exe" tool (supplied with AutoHotKey) can also assist with identifing screen coordinates. The 0/0 cooridinate (for x/y) is usually the top left corner of the "main display" as defined in the Display Settings app.

Normally each physical monitor is given a distinct section - however virtual "monitors" can also be defined - for example to test a multi-monitor setup on a laptop or smaller array. See the *Monitors_TestMode* .ini files for examples.

The Monitor ID Number (e.g. 1 to n...) is used in the Windows Config file to map windows to monitors. 

- Unique Monitor Section for each monitor [Monitor-*ID*] e.g. [Monitor-1]. The Monitor ID is used in Window definition sections to identify the destination of each window.
- key/key2 - Optional Shortcut Key(s) = key for hotkey assignment. Quickly move the cursor to the top left corner of the desired monitor.
    - ! = Alt + Key
    - ^ = Control + Key
    - \+ = Shift + Key
- width - Required: horizontal resolution of monitor
- height - Required: vertical resolution of monitor
- x - Required: horizontal coordinate of the top left corner of the monitor
- y - Required: vertical coordinate of the top left corner of the monitor

```
; ========== Example Monitors.ini File =====================================
[Monitor-1]
key=!1
width=1920
height=1080
x=0
y=-1080

[Monitor-2]
key=!2
width=1920
height=1080
x=1920
y=-1080
```

---

### Windows

The windows.ini file contains instructions to load apps/browser windows on to monitors as defined in the monitors.ini file. This is particually useful for multi-monitor configurations such as for Operation Centres. 

;	Description:
;		Configuration file defines the content to be displayed on a multimonitor configuration 
;		such as for video walls. 
;		Window-XX Section number must be unique - but not necessarily sequential
;		[replace following with external document] 

The following classes of windows are supported:
- URL	[Default] - Open simplified Browser window using URL supplied in SRC paramater
- CMD - Run command defined in SRC paramater
- Banner - Run defined Banner command (see globals.ini) with paramaters as defined in SRC paramater
- Spawned - Assume window is already open and reposition as defined (also used for RePosWindows input paramater)


	/*	Description:
		OffsetX = Horizontal Offset from Monitor Right
		OffsetY = Vertical Offset from Monitor Top (-ve Up)
		Width = Width of Window. Can be specific pixel value or "x1", "x0.5" etc. for multiple of parent monitor
		Height = Height of Window. Defined as above for Width.
		
		
		* Optional Paramaters: 
		* WinDef.case: 
			URL	[Default] - Open simplified Browser window using URL supplied in SRC paramater
			CMD - Run command defined in SRC paramater
			Banner - Run defined Banner command (see globals.ini) with paramaters as defined in SRC paramater
			Spawned - Assume window is already open and reposition as defined (also used for RePosWindows input paramater)
			
			
		* CloseExisting: Default = True		- Close Existing window if it exists
	*/


;	Configuration File for Window definitions - NEAC
;

;
;	Used by:
;		LoadWindows.ahk/exe to assign application windows to specific positions on a Video wall.
;
;	See Also:
;		Partner Config File = Monitor.ini
;
;	Tony Pack - September 2019
;
;	Modified for new Detcap
;		Bren Moushall & Tony Pack - April 2021
;
; 	VGA = 640 x 480
; 	HDTV = 1920 x 1080
;	
; 	[Section-Name] = Window + Number [Arbitary number - but must be unique]
;	TODO - expand variable reference
;
;	TODO - add reference to Monitors Setup instructions
; --------------------------------------------------------- 

; ========== Open Browser Windows ======================================
; SeisComP3/GEMPA Earthquake and Station Views & Earthquakes@GA-Australia

[Window-01]
; GEMPA Earthquake View
title=gempa eqview
alwaysOnTop=False
monitor=4
width=width+16
;height=height-(140-28)-28
;xOff=(140-28)-28
height=height
xOff=-8
yOff=0
case=URL
src=http://192.245.111.237/gaps/eqview/
;src=https://www.eatws.net/gaps/eqview/
; http://192.245.111.237/gaps/eqview/ This is the address needed for Tier2
sendcmd="{Enter}^+{Up}{Left}"

[Window-02]
;GEMPA Station View
title=gempa StationView
alwaysOnTop=False
monitor=9
width=width+16
height=height
xOff=-8
yOff=0
case=URL
src=http://192.245.111.237/gaps/stationview/
;src=https://www.eatws.net/gaps/stationview/ 
; http://192.245.111.237/gaps/stationview/ This is the address needed for Tier2


[Window-03]
; Earthquakes@GA - Zoom Australia
title=Earthquakes@GA-Australia
alwaysOnTop=False
monitor=5
width=width
height=height
xOff=0
yOff=0
case=URL
src=C:\W10Dev\OpsCentres\NEAC-Proc1\Earthquakes_Australia.html


; ========== Open 4 x Waveform Trace Windows ======================================

[Window-05]
; Open MobaXTerm on Desktop Window and establish connections
title=Tier2_Desktop_VM
alwaysOnTop=False
monitor=11
width=width*0.8
height=height*0.8
xOff=height*0.1
yOff=width*0.1
case=CMD
src=C:\Program Files (x86)\Mobatek\MobaXterm\MobaXterm.exe -bookmark "Tier2_Desktop_VM"

[Window-06]
; Recent Activity Waveform Trace
;-------------------------------
title=scrttv@127.0.0.1:28180@ubuntu
;title=scrttv@127.0.0.1:4805@Tier2_Desktop_VM.
;title=scrttv@127.0.0.1:4805@Ubuntu-VM
alwaysOnTop=False
monitor=1
width=width+16
height=height-80
; Banner Height = 120
; Header Bar = 33 pixels
xOff=-8
yOff=80
case=CMD
src=C:\Program Files (x86)\Mobatek\MobaXterm\MobaXterm.exe -bookmark "Tier2_Desktop_VM (all_feeds)"
; Sleep 20 seconds = 20000 milliseconds
WindowSleep=30000


## Companion Applications

### MouseKeys

Designed to allow users to quickly locate or to move the mouse cursor.

Uses the shortcut keys as defined in the Monitors.ini config file.

### AntiSleep

The AntiSleep script...


### 