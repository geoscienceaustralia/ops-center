; 	LoadWindows.ahk
;	
;	Tony Pack - January/July 2018
;	Update:
;		TP - September 2019 - modified to load from configuration files and evaluate window dimensions via equations
;
;	Utility for multiscreen display. Loads monitor and window definitions from .ini file and opens windows as defined.
;
;	Requires:
;		Globals.ini
;		Monitors.ini - and test versions: Monitors_TestMode1.ini and Monitors_TestMode2.ini
;		Windows.ini
;
;	Command Line Parameters:
;		
;		"--help" or "-h" to load help screen
;
;	===========================================================================

	HelpStr := "Load a defined series of windows"
	. "`n  /G	Load Globals File (e.g. /G.\Globals.ini) - Default: .\Globals.ini"
	. "`n  /M 	Load Monitor Definition File - Default: defined in Globals.ini file"
	. "`n  /W 	Load Windows Definition File - Default: defined in Globals.ini file"
	. "`n  /D 	Set Debug Status - Default: defined in Globals.ini file"
	. "`n  /R 	Reposition windows as defined in Windows Definition File without closing/restarting."
	
;	TODO:
;	need to add additional tests to address dead windows... in WinWait/WinActivate
;
;	---------------------------------------------------------------------------

	#SingleInstance force
	#Include FunctionLib.ahk		; Include Shared Function Library
	
	SetTitleMatchMode, 2			; Title Match Mode = can contain text anywhere in title
	global WinExcludeList := ""		; List of windows already loaded - to cater for duplicate window names
	global GlobalsFile := ".\Globals.ini"
	global WinCoords := []		; Size and Coordinates for Windows
	global WinDef := []				; Definitions for displayed Windows
	

	
;	Define Global Values for use by Config File
;	===========================================================================
	
	;[Config_Files]
	global MonitorsFile				; define dimensions and layout of monitors
	global WindowsFile				; define content of windows to display
	global RePosWindows				; if True - then ignore cmd paramater of each window and repositon existing
	
	;[Initiatalisation]
	global WinWaitTime				; wait time for WinWait command
	global TitleMatchMode			; SetTitleMatchMode: 1 = Starts With, 2 = Contains, 3 = Exact Match
	
	;[Debug]
	global Debug					; turn on general Debugging
		; 1 = General Debuggin on.
		; 2 = turn on detailed Debugging
		; 4 = turn on Debugging for Configuration
		; 8 = turn on Debugging for DebugDisplay
		; 16 = turn on Debugging for DebugMonitorsInit
		; 32 = turn on Debugging for DebugWindowInit	
		; 64 = turn on Debugging for DebugWindowDisplay
		;
	global DebugLev2				; turn on detailed Debugging
	global DebugConfig				; turn on Debugging for Configuration
	global DebugDisplay				; turn on Debugging for 
	global DebugMonitorsInit		; turn on Debugging for 
	global DebugWindowInit			; turn on Debugging for 	
	global DebugWindowDisplay		; turn on Debugging for 
	
	;[BrowserCMD]
	global BrowserCMD				; Command used to open browser windows (eg Google Chrome). To be followed by the URL
	global BrowserCMDSuffix			; Any suffix command to be added after the window URL
	
	;[BannerCMD]
	global BannerCMD
	
;	Check if any paramaters have been received from the command line
;	===========================================================================
;
;	Parameters:
;		"D" or "/D"	- Define Directory for config files [ToDo]
;		"G=" or "/G" - Overwrite Globals File
;		"M=" or "/M" - Overwrite Monitors File
;		"W=" or "/W" - Overwrite Windows File
;		"/R" => Reposition Windows: Reposition Windows without running command component
;		"/D" - Debug: 
;			1 or "=1"	Debug Level 1
;			2 or "=2"	Debug Level 2
;
;	Arguments: LoadWindows.exe G=GlobalsConfigFile M=MonitorsConfigFile W=WindowsConfigFile
;		global MonitorsFile
;		global WindowsFile
;		global RePosWindows

;	Calls from Load Globals
;		IniRead, MonitorsFile, %GlobalsFile%, %A_LoopField%, Monitors
;		IniRead, WindowsFile, %GlobalsFile%, %A_LoopField%, Windows

	for n, param in A_Args  ; For each parameter:
	{
		If InStr(param, "G=", ,1) or InStr(param, "/G", ,1)			; Overwrite Globals File
		{
			MsgBox % "Globals File Paramater Detected"
			GlobalsFile := SubStr(param, 3)
		}
		Else If InStr(param, "M=", ,1) or InStr(param, "/M", ,1)	; Overwrite Monitors File
		{
			MsgBox % "Monitors File Paramater Detected"
			MonitorsFile_Param := SubStr(param, 3)
		}
		Else If InStr(param, "W=", ,1) or InStr(param, "/W", ,1)	; Overwrite Windows File
		{
			WindowsFile_Param := SubStr(param, 3)
			MsgBox % "Windows File Paramater Detected " . WindowsFile_Param
		}
		Else If InStr(param, "/R", ,1)	; RePosition Windows
		{
			MsgBox % "RePosWindows Flag Detected"
			RePosWindows := True
		}
		Else If InStr(param, "/D", ,1)	; Set Debug State
		{
			Debug_Param := SubStr(param, 3)
			MsgBox % "Debug Flag Detected " . Debug_Param
			If (Debug_Param = 1) OR (Debug_Param = "=1")
			{
				MsgBox % "Setting Debug to True"
				Debug := True
			}
			Else If (Debug_Param = 2) OR (Debug_Param = "=2")
			{
				MsgBox % "Setting Debug and DebugLev2 to True"
				Debug := True
				DebugLev2 := True
			}
		}
		Else If InStr(param, "--help", ,1) or InStr(param, "-h")	; Display help info
		{
			MsgBox % HelpStr
			ExitApp
		}					
	}

;	Popup Loading Banner:
;	===========================================================================
	SubText := "Configuration Files: `n Globals = " . GlobalsFile . "`n Monitor = " . MonitorsFile_Param . "`n Window = " . WindowsFile_Param

	Progress, zh0 fs18, Loading Windows
	Sleep, 4000
	Progress, Off

	If Debug
		MsgBox % "Configuration Files: `n Globals = " . GlobalsFile 
		. "`n Monitor = " . MonitorsFile_Param 
		. "`n Window = " . WindowsFile_Param
		. "`n Debug = " . Debug
		. "`n DebugLev2 = " . DebugLev2
		. "`n RePositioning = " . RePosWindows
		
		
;	Load Global Values from Config File
;	===========================================================================
;	GlobalsFile defined above

	If !FileExist(GlobalsFile)
	{
		MsgStr := "No Initialisation File. Please Create the file Globals.ini"
			. "`n  See Readme.txt for Information"
			. "`n  LoadWindows.exe will exit"
		MsgBox % MsgStr		
		ExitApp
	} 									

	LoadGlobals(GlobalsFile)

	; Overwrite values for Monitor and/or Window config files if passed as paramaters
	If !(MonitorsFile_Param = "")
		MonitorFile := MonitorsFile_Param
		
	If !(WindowsFile_Param = "")
		WindowsFile := WindowsFile_Param


;	---------- END Load Globals -----------------------------------------------

	CheckDisplays()			; Check attached monitor dimensions - Set Test/Live Mode
	
	LoadMonitorsArray()		; Loads WinCoords array with definitions of available Monitors

	LoadWindowsArray()		; Loads WinDef array with definitions of windows to be displayed on Monitors


;	Open Windows:
;	===========================================================================
;	Load each Window from WinDef array and open
	
	For WindowID in WinDef
	{
		If DebugWindowDisplay 
			{
				MsgStr := "`n  Loading Window: " . WindowID . " - " . WinDef[WindowID].Title
					. "`n  AlwaysOnTop: " . WinDef[WindowID].alwaysOnTop
					. "`n  Case: " . WinDef[WindowID].case
					. "`n  SRC: " . WinDef[WindowID].src
					. "`n  SendCMD: " . WinDef[WindowID].sendcmd
					. "`n  Monitor: " . WinDef[WindowID].monitor
					. "`n  OffsetX: " . WinDef[WindowID].OffsetX
					. "`n  OffsetY: " . WinDef[WindowID].OffsetY
					. "`n  width: " . WinDef[WindowID].width
					. "`n  height: " . WinDef[WindowID].height
					. "`n  LoginTitle: " . WinDef[WindowID].LoginTitle
					. "`n  LoginSendcmd: " . WinDef[WindowID].LoginSendcmd
					. "`n  WindowSleep: " . WinDef[WindowID].WindowSleep
					. "`n  WindowMsg: " . WinDef[WindowID].WindowMsg
			
				MsgBox % "Window Details [Read Array]: " . MsgStr
			}

		Display_Window(WinDef[WindowID])
	}
;	---------- END Open Windows -----------------------------------------------


;	Popup Finished Banner:
;	===========================================================================
	Progress, zh0 fs18, Load Windows Complete.
	Sleep, 4000
	Progress, Off

ExitApp


	
;	Functions:
;	===========================================================================
	
	
Display_Window(WinDef)
{	
	/* 	TODO:
		? why is there a difference between "monitor" and "Window" sizes - ?? AHK Setting...
	*/
	
	/*	Description:
		OffsetX = Horizontal Offset from Monitor Right
		OffsetY = Vertical Offset from Monitor Top (-ve Up)
		Width = Width of Window. Can be specific pixel value or "x1", "x0.5" etc. for multiple of parent monitor
		Height = Height of Window. Defined as above for Width.
		
		
		* Optional Paramaters: 
		* WinDef.case: 
			URL	[Default] - Open simplified Browser window using URL supplied in SRC paramater
			CMD - Run command defined in SRC paramater
			Banner - Run defined Banner command (see globals.ini) with paramaters as defined in SRC paramater
			Spawned - Assume window is already open and reposition as defined (also used for RePosWindows input paramater)
			
			
		* CloseExisting: Default = True		- Close Existing window if it exists
	*/
	
	If (Debug OR DebugWindowDisplay)
		MsgBox % "Openening Window: " . WinDef.Title

		If DebugWindowDisplay 
			{
				MsgStr := "`n  Loading Window: " . WinDef.Title
					. "`n  AlwaysOnTop: " . WinDef.alwaysOnTop
					. "`n  Case: " . WinDef.case
					. "`n  SRC: " . WinDef.src
					. "`n  SendCMD: " . WinDef.sendcmd
					. "`n  Monitor: " . WinDef.monitor
					. "`n  OffsetX: " . WinDef.OffsetX
					. "`n  OffsetY: " . WinDef.OffsetY
					. "`n  width: " . WinDef.width
					. "`n  height: " . WinDef.height
					. "`n  LoginTitle: " . WinDef.LoginTitle
					. "`n  LoginSendcmd: " . WinDef.LoginSendcmd
					. "`n  WindowSleep: " . WinDef.WindowSleep
					. "`n  WindowMsg: " . WinDef.WindowMsg
			
				MsgBox % "Window Details [Read Array]: " . MsgStr
			}
	; ---------------- WinDef CASE ----------------		
	If (WinDef.case = "URL")
	{
		RunCMD := BrowserCMD . WinDef.src . BrowserCMDSuffix
	}
	Else If (WinDef.case = "CMD")
		RunCMD := WinDef.src
	Else If (WinDef.case = "Banner")
	{
		RunCMD := BannerCMD . WinDef.src 
	}
	If DebugWindowDisplay
		MsgBox % "RunCMD: " . RunCMD
	
	; ---------------- Load WinDef Paramaters----------------		
	WinTitle := WinDef.Title
	WinWidth := WinDef.Width
	WinHeight := WinDef.Height
	WinOffsetX := WinDef.OffsetX
	WinOffsetY := WinDef.OffsetY
	WinSendCMD := WinDef.sendcmd
	WinLoginTitle := WinDef.LoginTitle
	WinLoginSendcmd := WinDef.LoginSendcmd
	WinSleep := WinDef.WindowSleep
	WinMsg := WinDef.WindowMsg
	

	; ---------------- Parse WinDef Paramaters ----------------		
	if WinWidth is not number		; Evaluate as equation
	{
		equation := ParseEquation(WinWidth, WinDef.Monitor)
		WinWidth := Eval("floor(" . equation . ")")
	}
	
	if WinHeight is not number		; Evaluate as equation
	{
		equation := ParseEquation(WinHeight, WinDef.Monitor)
		WinHeight := Eval("floor(" . equation . ")")
	}
	
	if WinOffsetX is not number		; Evaluate as equation
	{
		equation := ParseEquation(WinOffsetX, WinDef.Monitor)
		WinOffsetX := Eval("floor(" . equation . ")")
	}
	
	if WinOffsetY is not number		; Evaluate as equation
	{
		equation := ParseEquation(WinOffsetY, WinDef.Monitor)
		WinOffsetY := Eval("floor(" . equation . ")")
	}	

	TopX := WinCoords[WinDef.Monitor].x + WinOffsetX
	TopY := WinCoords[WinDef.Monitor].y + WinOffsetY
	
	If DebugWindowDisplay 
	{
		MsgStr := "`n  Title: " . WinTitle
			. "`n  AlwaysOnTop: " . WinDef.alwaysOnTop
			. "`n  Case: " . WinDef.case
			. "`n  SRC: " . WinDef.src
			. "`n  SendCMD: " . WinDef.sendcmd
			. "`n  Monitor: " . WinDef.monitor
			. "`n  TopX: " . TopX
			. "`n  TopY: " . TopY
			. "`n  width: " . WinWidth
			. "`n  height: " . WinHeight
			. "`n  Run: " . RunCMD
			. "`n  LoginTitle: " . WinLoginTitle
			. "`n  LoginSendcmd: " . WinLoginSendcmd
			. "`n  WindowSleep Time: " . WinSleep
			. "`n  WindowMsg: " . WinMsg
	
		MsgBox % "Window Details [Window Open]: " . MsgStr
	}
	
	; ---------------- WinDef CASE = Banner ----------------		
	If (WinDef.case = "Banner")				; Add additional paramaters from standard config
	{
		If !(WinTitle = "")
			RunCMD .= " /L='" . WinTitle . "'"
		
		If !(WinWidth = "")
			RunCMD .= " /W=" . WinWidth
			
		If !(WinHeight = "")
			RunCMD .= " /H=" . WinHeight

		If !(WinOffsetX = "")
			RunCMD .= " /X=" . TopX

		If !(WinOffsetY = "")
			RunCMD .= " /Y=" . TopY
		
		RunCMD := StrReplace(RunCMD, "'", """")

		If Debug	
			MsgBox % "RunCMD: " . RunCMD
	}

	; ---------------- Close Existing Window ----------------	
	If WinExist(WinTitle)	
	{
		If !(WinDef.case = "Spawned" or RePosWindows) 
		{
			If Debug
				MsgBox % "Window: " . WinTitle . " Existing - will now close"
			WinClose
		}
	}

	; ---------------- Open Window If Not Already Open ----------------	
	; 
	If !WinExist(WinTitle)	; Check that window is not already open - if not then open
	{
		If Debug	
			MsgBox % "Window: " . WinTitle . " Does not already exist - will now Open"

	; ******************** 
		If !(WinDef.case = "Spawned" or RePosWindows) 
			Run, %RunCMD%

		if (WinSleep > 0)
		{
			If Debug	
				MsgBox % "In Sleep Function: WinSleep: " . WinSleep 		; . " RePosWindows: " . RePosWindows
				
			Sleep, %WinSleep%
		}
		; AND !RePosWindows
		
		If !(WinLoginTitle ="")		
		{
			; add both windows names to a group - to check if either window appears
			GroupAdd, TempGroup, %WinTitle%
			GroupAdd, TempGroup, %WinLoginTitle%

			WinWait, ahk_group TempGroup, , %WinWaitTime%
			
			if WinExist(WinLoginTitle) and !(WinLoginSendcmd = "")		; if the Login Window appears, then send cmd.
			{
				Sleep, 1000
				Send, %WinLoginSendcmd%
			}
		}
		Else 
			WinWait, %WinTitle%, , %WinWaitTime%

		if (WinDef.AlwaysOnTop="True") AND !(WinDef.case = "Banner")	; Already Set for Banners
			WinSet, AlwaysOnTop

		if !(WinSendCMD = "")
			Sleep, 1000
			Send, %WinSendCMD%
		
		If DebugWindowDisplay
		{
			if ErrorLevel   ; i.e. it's not blank or zero.
				MsgBox % "The window " . WinTitle . " does not exist. " . ErrorLevel
			else
				MsgBox % "The Window " . WinTitle . " has been created."
		}	
		WinActivate	
	}
	
	If WinMsg
	{
		MsgBox,48,Loading Windows, % WinMsg
	}

	If WinExist(WinTitle) AND !(WinDef.case = "Banner")		; Banners already moved to correct placement
	{
		If DebugWindowDisplay
			MsgBox % "WinMove: " . WinTitle . ", " . TopX . ", " . TopY . ", " . WinWidth . ", " . WinHeight

		WinActivate  	; Automatically uses the window found above.
		WinMove, %WinTitle%, , %TopX%, %TopY%, %WinWidth%, %WinHeight%
	}
}
;	---------- End Function: Display_Window -----------------------------------


ParseEquation(equation, monitor)		; Pases Equation and replaces with Monitor dimensions
{
	equation := StrReplace(equation, "width" , WinCoords[monitor].width)
	equation := StrReplace(equation, "height" , WinCoords[monitor].height)
		
	return equation
}
	
;	---------- End Function: ParseEquation ------------------------------------
		
